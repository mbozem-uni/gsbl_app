package de.ovgu.de.gsbl.settings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.couchbase.lite.CouchbaseLiteException;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.MultiSelectListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceGroup;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.util.Log;
import android.widget.Toast;
import de.ovgu.de.gsbl.Application;
import de.ovgu.de.gsbl.R;
import de.ovgu.de.gsbl.datamodel.Begriff;
import de.ovgu.de.gsbl.datamodel.Bezeichnung;
import de.ovgu.de.gsbl.view.datamodel.Field;
import de.ovgu.de.gsbl.view.datamodel.View;
import de.ovgu.de.gsbl.view.datamodel.ViewCreation;
import de.ovgu.de.gsbl.view.util.DataGenerator;
import de.ovgu.de.gsbl.view.util.Kurz2LangBezeichnung;
import de.ovgu.de.gsbl.view.util.Kurz2LangBezeichnungMap;

public class SettingsActivity extends PreferenceActivity {

	private static SettingsActivity reference;
	private static final String TAG = "SettingsActivity";

	private static final String preferenceKeyPrefix = "prefChooseSearchItems";

	private static final String preferenceScreenForSearchCriteria = "preferenceScreenForSearchCriteria";

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		reference = this;

		// Oberster Screen f�r die Preferences
		PreferenceScreen root = getPreferenceManager().createPreferenceScreen(
				this);
		this.setPreferenceScreen(root);

		// Erste Kategorie: Zum Festlegen der View
		PreferenceCategory categoryForView = new PreferenceCategory(this);
		categoryForView.setTitle(getString(R.string.pref_view));
		root.addPreference(categoryForView);

		ListPreference listOfViews = new ListPreference(this);
		listOfViews.setTitle(getString(R.string.pref_choose_view));
		listOfViews.setSummary(getString(R.string.pref_choose_view_summary));
		listOfViews.setKey("prefChooseView");
		listOfViews.setDialogTitle(getString(R.string.pref_choose_view));

		categoryForView.addPreference(listOfViews);

		final ListPreference listPreference = (ListPreference) findPreference("prefChooseView");
		listPreference.setDefaultValue("0");
		initListPreferenceData(listPreference);

		listPreference
				.setOnPreferenceClickListener(new OnPreferenceClickListener() {
					@Override
					public boolean onPreferenceClick(Preference preference) {

						setListPreferenceData(listPreference);
						return false;
					}
				});

		// Zweite Kategorie zum Festlegen der Suchkriterien
		PreferenceCategory categoryForSearchFields = new PreferenceCategory(
				this);
		categoryForSearchFields.setTitle(getString(R.string.search_settings));

		root.addPreference(categoryForSearchFields);

		PreferenceScreen prefScreenForSearchFields = getPreferenceManager()
				.createPreferenceScreen(this);
		prefScreenForSearchFields
				.setOnPreferenceClickListener(new OnPreferenceClickListener() {

					@Override
					public boolean onPreferenceClick(Preference preference) {
						// es muss gepr�ft werden, ob Items angehakt werden
						// k�nnen
						checkNumberOfCheckedPreferences(null, 0);
						return true;
					}
				});
		categoryForSearchFields.addPreference(prefScreenForSearchFields);
		prefScreenForSearchFields.setTitle(getString(R.string.search_settings));
		prefScreenForSearchFields
				.setSummary(getString(R.string.search_items_choose));
		prefScreenForSearchFields.setKey(preferenceScreenForSearchCriteria);

		this.addAllPreferences(prefScreenForSearchFields);

	}

	@Override
	protected void onPause() {
		super.onPause();
		Application gsblApp = (Application) getApplication();
		List<Begriff> selectedSearchItems = getBegriffeFromPreferences();
		gsblApp.getDatabase().setSearchableBegriffe(selectedSearchItems);
	}

	/**
	 * @return liefert die ausgewählten Begriffe aus den
	 *         Suchkriterien-Einstellungen
	 */
	private List<Begriff> getBegriffeFromPreferences() {
		List<Begriff> selectedSearchItems = new ArrayList<Begriff>();
		// die SearchItems entsprechen den ausgewaehlten Preferences
		SharedPreferences prefManager = PreferenceManager
				.getDefaultSharedPreferences(this);

		Map<String, List<String>> oberUndUnterbegriffe = new HashMap<String, List<String>>();
		Map<String, ?> prefs = prefManager.getAll();
		for (String pref : prefs.keySet()) {
			if (pref.startsWith(preferenceKeyPrefix)
					&& (Boolean) prefs.get(pref)) {
				// Die Preferences für die Suche werden nach dem Muster
				// prefChooseSearchItems.Merkmal.Feld gespeichert
				String[] prefNameParts = pref.split("\\.");
				String oberbegriff = prefNameParts[1];
				String unterbegriff = prefNameParts[2];
				if (oberUndUnterbegriffe.containsKey(oberbegriff)) {
					oberUndUnterbegriffe.get(oberbegriff).add(unterbegriff);
				} else {
					List<String> unterbegriffe = new ArrayList<String>();
					unterbegriffe.add(unterbegriff);
					oberUndUnterbegriffe.put(oberbegriff, unterbegriffe);
				}
			}
		}

		for (String oberbegriff : oberUndUnterbegriffe.keySet()) {
			Begriff begriff = new Begriff(oberbegriff);
			for (String unterbegriff : oberUndUnterbegriffe.get(oberbegriff)) {
				begriff.addUnterbegriff(new Begriff(unterbegriff));
			}
			selectedSearchItems.add(begriff);
		}
		return selectedSearchItems;
	}

	/**
	 * @param parentPreference
	 *            kann ein PreferenceScreen sein, dem weitere
	 *            PreferenceCategories zugeordnet werden; kann aber auch eine
	 *            PreferenceCategory sein, der einzelne Elemente zugeordnet
	 *            werden
	 * @param begriffe
	 * @param kurz2LangBezeichnung
	 */
	private void addAllPreferences(PreferenceGroup parentPreference) {
		Application app = (Application) getApplication();
		Kurz2LangBezeichnungMap langBezeichnungen;
		try {
			langBezeichnungen = app.getDatabase().getKurz2LangBezeichnungMap();
			Kurz2LangBezeichnung kurz2LangBezeichnung = new Kurz2LangBezeichnung(
					langBezeichnungen);

			addAllPreferences(parentPreference, langBezeichnungen);

		} catch (CouchbaseLiteException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	private void addAllPreferences(PreferenceGroup parentPreference,
			Kurz2LangBezeichnungMap langBezeichnungenErsteEbene) {

		for (Bezeichnung bezeichnungErsteEbene : langBezeichnungenErsteEbene
				.keySet()) {
			PreferenceCategory prefCatErsteEbene = new PreferenceCategory(
					reference);
			prefCatErsteEbene.setTitle(bezeichnungErsteEbene
					.getLangBezeichnung());
			parentPreference.addPreference(prefCatErsteEbene);

			Kurz2LangBezeichnungMap langBezeichnungenZweiteEbene = langBezeichnungenErsteEbene
					.get(bezeichnungErsteEbene);
			for (Bezeichnung bezeichnungZweiteEbene : langBezeichnungenZweiteEbene
					.keySet()) {
				PreferenceScreen prefScreenZweiteEbene = getPreferenceManager()
						.createPreferenceScreen(this);
				prefScreenZweiteEbene.setTitle(bezeichnungZweiteEbene
						.getLangBezeichnung());
				prefScreenZweiteEbene
						.setLayoutResource(R.layout.activity_settings);
				prefCatErsteEbene.addPreference(prefScreenZweiteEbene);

				// Dritte Ebene = Merkmalsebene
				Kurz2LangBezeichnungMap langBezeichnungenDritteEbene = langBezeichnungenZweiteEbene
						.get(bezeichnungZweiteEbene);
				// falls die dritte Ebene keine Felder als Unterordnung hat,
				// müssen die Merkmal der Dritten Ebene schon als Checkboxen
				// eingebunden werden
				for (Bezeichnung bezeichnungDritteEbene : langBezeichnungenDritteEbene
						.keySet()) {
					if (langBezeichnungenDritteEbene
							.get(bezeichnungDritteEbene).isEmpty()) {

						CheckBoxPreference preferenceDritteEbene = new CheckBoxPreference(
								this);
						preferenceDritteEbene.setTitle(bezeichnungDritteEbene
								.getLangBezeichnung());
						preferenceDritteEbene.setKey("prefChooseSearchItems."
								+ bezeichnungZweiteEbene.getKurzBezeichnung()
								+ "."
								+ bezeichnungDritteEbene.getKurzBezeichnung());
						preferenceDritteEbene
								.setLayoutResource(R.layout.activity_settings);
						preferenceDritteEbene
								.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

									@Override
									public boolean onPreferenceChange(
											Preference changedPreference,
											Object newValue) {
										int selectedSearchItems;
										if ((Boolean) newValue) {
											selectedSearchItems = 1;
										} else {
											selectedSearchItems = -1;
										}
										checkNumberOfCheckedPreferences(
												(CheckBoxPreference) changedPreference,
												selectedSearchItems);
										return true;
									}

								});

						prefScreenZweiteEbene
								.addPreference(preferenceDritteEbene);

					} else {
						PreferenceCategory prefCatDritteEbene = new PreferenceCategory(
								reference);
						prefCatDritteEbene.setTitle(bezeichnungDritteEbene
								.getLangBezeichnung());
						prefScreenZweiteEbene.addPreference(prefCatDritteEbene);
						// Vierte Ebene = Feldebene
						addFieldsAsCheckBoxPreference(
								langBezeichnungenDritteEbene
										.get(bezeichnungDritteEbene),
								bezeichnungDritteEbene, prefCatDritteEbene);
					}
				}
			}
		}

	}

	/**
	 * prefGroup kann ein PreferenceScreen sein oder eine CheckBoxPreference
	 * 
	 * @param k2lBezeichnungMapLetzteEbene
	 * @param bezeichnungVorletzteEbene
	 * @param parentPrefGroup
	 */
	private void addFieldsAsCheckBoxPreference(
			Kurz2LangBezeichnungMap k2lBezeichnungMapLetzteEbene,
			Bezeichnung bezeichnungVorletzteEbene,
			PreferenceGroup parentPrefGroup) {
		for (Bezeichnung bezeichnungLetzteEbene : k2lBezeichnungMapLetzteEbene
				.keySet()) {
			CheckBoxPreference preferenceVierteEbene = new CheckBoxPreference(
					this);
			preferenceVierteEbene.setTitle(bezeichnungLetzteEbene
					.getLangBezeichnung());
			preferenceVierteEbene.setKey("prefChooseSearchItems."
					+ bezeichnungVorletzteEbene.getKurzBezeichnung() + "."
					+ bezeichnungLetzteEbene.getKurzBezeichnung());
			preferenceVierteEbene.setLayoutResource(R.layout.activity_settings);
			preferenceVierteEbene
					.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

						@Override
						public boolean onPreferenceChange(
								Preference changedPreference, Object newValue) {
							int selectedSearchItems;
							if ((Boolean) newValue) {
								selectedSearchItems = 1;
							} else {
								selectedSearchItems = -1;
							}
							checkNumberOfCheckedPreferences(
									(CheckBoxPreference) changedPreference,
									selectedSearchItems);
							return true;
						}

					});

			parentPrefGroup.addPreference(preferenceVierteEbene);

		}
	}

	private void checkNumberOfCheckedPreferences(
			CheckBoxPreference changedPreference, int selectedSearchItems) {
		selectedSearchItems = selectedSearchItems + countSelectedSearchItems();
		Toast.makeText(reference,
				selectedSearchItems + " von maximal 8 gewählt",
				Toast.LENGTH_SHORT).show();
		if (selectedSearchItems > 7) {
			enablePreferences(false);
			if (changedPreference != null) {
				changedPreference.setSelectable(true);
			}
		}
		if (selectedSearchItems < 8) {
			enablePreferences(true);
		}
	}

	/**
	 * Macht die CheckBoxPreferences selectable bzw. nicht selectable, die nicht
	 * ausgewählt sind. Das Nicht-Selectable-Schalten ist notwendig, falls
	 * bereits 8 Felder ausgewählt wurden.
	 * 
	 * @param enable
	 */
	private void enablePreferences(boolean enable) {

		for (CheckBoxPreference checkBoxPreference : getAllCheckBoxPreferenceForSearchCriteria()) {
			if (enable) {
				checkBoxPreference.setSelectable(true);
			} else {
				if (!checkBoxPreference.isChecked()) {
					checkBoxPreference.setSelectable(false);
				}
			}
		}
	}

	/**
	 * Liefert eine Liste mit den CheckBoxPreferences dieser App. Diese beginnen
	 * alle mit "prefChooseSearchItems"
	 * 
	 * @return
	 */
	private List<CheckBoxPreference> getAllCheckBoxPreferenceForSearchCriteria() {
		List<CheckBoxPreference> checkBoxPrefs = new ArrayList<CheckBoxPreference>();

		// Erste Ebene
		PreferenceScreen prefScreen = (PreferenceScreen) findPreference(preferenceScreenForSearchCriteria);
		for (int h = 0; h < prefScreen.getPreferenceCount(); h++) {
			PreferenceCategory prefCatErsteEbene = (PreferenceCategory) prefScreen
					.getPreference(h);
			// Zweite Ebene
			for (int i = 0; i < prefCatErsteEbene.getPreferenceCount(); i++) {
				PreferenceScreen prefScreenZweiteEbene = (PreferenceScreen) prefCatErsteEbene
						.getPreference(i);
				// Dritte Ebene, kann Merkmal oder Feld sein
				for (int j = 0; j < prefScreenZweiteEbene.getPreferenceCount(); j++) {
					// Fall 1: Dritte Ebene Merkmal mit darunter liegenden
					// Feldern
					if (prefScreenZweiteEbene.getPreference(j) instanceof PreferenceCategory) {
						PreferenceCategory prefCatDritteEbene = (PreferenceCategory) prefScreenZweiteEbene
								.getPreference(j);
						// Vierte Ebene
						for (int k = 0; k < prefCatDritteEbene
								.getPreferenceCount(); k++) {
							CheckBoxPreference checkBoxPrefVierteEbene = (CheckBoxPreference) prefCatDritteEbene
									.getPreference(k);
							checkBoxPrefs.add(checkBoxPrefVierteEbene);
						}
					} else {
						// Fall 2: Dritte Ebene Felder-Ebene
						CheckBoxPreference checkBoxPrefVierteEbene = (CheckBoxPreference) prefScreenZweiteEbene
								.getPreference(j);
						checkBoxPrefs.add(checkBoxPrefVierteEbene);
					}
				}
			}
		}

		return checkBoxPrefs;
	}

	private int countSelectedSearchItems() {
		int selectedSearchItems = 0;
		SharedPreferences prefManager = PreferenceManager
				.getDefaultSharedPreferences(this);
		Map<String, ?> prefs = prefManager.getAll();

		for (String pref : prefs.keySet()) {
			if (pref.startsWith("prefChooseSearchItems")
					&& (Boolean) prefs.get(pref)) {
				selectedSearchItems++;
			}
		}
		return selectedSearchItems;
	}

	@SuppressWarnings("deprecation")
	protected static void setListPreferenceData(ListPreference listPreference) {
		List<View> views = ViewCreation.getInstance().getCreatedViews();
		if (views != null && !views.isEmpty()) {
			CharSequence[] entries = new CharSequence[views.size()];
			CharSequence[] entryValues = new CharSequence[views.size()];
			for (int i = 0; i < views.size(); i++) {
				entries[i] = views.get(i).getName();
				entryValues[i] = String.valueOf(i);
			}
			listPreference.setEntries(entries);
			listPreference.setEntryValues(entryValues);
		} else {
			CharSequence[] entries = new CharSequence[0];
			CharSequence[] entryValues = new CharSequence[0];
			listPreference.setEntries(entries);
			listPreference.setEntryValues(entryValues);

			AlertDialog alertDialog = new AlertDialog.Builder(reference)
					.create();

			alertDialog.setTitle("Fehlerhafte Aktion");
			alertDialog
					.setMessage("Bitte erstellen oder laden Sie mindestens eine Sicht, um eine Standardsicht festlegen zu können.");
			alertDialog.setIcon(R.drawable.warning);

			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					reference.finish();
				}
			});
			alertDialog.show();
		}
	}

	protected static void initListPreferenceData(ListPreference listPreference) {
		List<View> views = ViewCreation.getInstance().getCreatedViews();
		if (views != null && !views.isEmpty()) {
			CharSequence[] entries = new CharSequence[views.size()];
			CharSequence[] entryValues = new CharSequence[views.size()];
			for (int i = 0; i < views.size(); i++) {
				entries[i] = views.get(i).getName();
				entryValues[i] = String.valueOf(i);
			}
			listPreference.setEntries(entries);
			listPreference.setEntryValues(entryValues);
		} else {
			CharSequence[] entries = new CharSequence[0];
			CharSequence[] entryValues = new CharSequence[0];
			listPreference.setEntries(entries);
			listPreference.setEntryValues(entryValues);
		}
	}
}
