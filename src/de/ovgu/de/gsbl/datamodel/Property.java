package de.ovgu.de.gsbl.datamodel;

import java.io.Serializable;
import java.util.List;

import de.ovgu.de.gsbl.view.datamodel.Field;

public class Property implements Serializable {

	private static final long serialVersionUID = 91307616868075367L;
	private String name;
	private List<Field> fields;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Field> getFields() {
		return fields;
	}

	@Override
	public String toString() {
		return "Property [name=" + name + ", fields=" + fields + "]";
	}

	public void setFields(List<Field> fields) {
		this.fields = fields;
	}

	public Property(String name, List<Field> fields) {
		super();
		this.name = name;
		this.fields = fields;
	}

	public Property() {
	}
}
