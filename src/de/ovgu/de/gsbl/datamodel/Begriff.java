package de.ovgu.de.gsbl.datamodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Klasse zur Darstellung der GSBL-Daten-Hierarchie. Hiermit k�nnen alle
 * Hierarchieebenen von den Oberbegriffen bis zu den Merkmalen und Feldern
 * dargestellt werden.
 * 
 * @author Christian
 * 
 */
public class Begriff implements Serializable {

	private static final long serialVersionUID = -2584744092901018964L;
	/**
	 * Ist die Kurzbezeichnung dieses Begriffes 
	 */
	private String name;
	private List<Begriff> unterbegriffe;

	public Begriff(String name) {
		this.name = name;
	}

	public Begriff(String name, List<Begriff> unterbegriffe) {
		this.name = name;
		this.unterbegriffe = unterbegriffe;
	}
	
	public String getName() {
		return name;
	}

	public List<Begriff> getUnterbegriffe() {
		if (unterbegriffe == null)
			unterbegriffe = new ArrayList<Begriff>();
		return unterbegriffe;
	}

	public void addUnterbegriff(Begriff unterbegriff) {
		this.getUnterbegriffe().add(unterbegriff);
	}

	/**
	 * Gibt an, ob es sich bei diesem Begriff um ein Feld, d.h. die letzte Ebene
	 * in der GSBL-Hierarchie handelt.
	 * 
	 * @return
	 */
	public boolean isFeld() {
		return this.unterbegriffe == null || this.unterbegriffe.isEmpty(); 
	}
	
	public List<String> collectNames() {
		List<String> names = new ArrayList<String>();
		names.add(name);
		for (Begriff b : getUnterbegriffe()) {
			names.addAll(b.collectNames());
		}
		return names;
	}

	@Override
	public String toString() {
		return "Begriff [name=" + name + ", unterbegriffe=" + unterbegriffe
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((unterbegriffe == null) ? 0 : unterbegriffe.hashCode());
		return result;
	}
	
	
	
}
