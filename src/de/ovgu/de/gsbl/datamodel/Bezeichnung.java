package de.ovgu.de.gsbl.datamodel;

import java.io.Serializable;

/**
 * POJO das eine Kurz- und eine Langbezeichnung für einen Begriff hält
 * 
 * @author Christian
 * 
 */
public class Bezeichnung implements Serializable {

	private static final long serialVersionUID = -8273740030593711000L;
	
	private String kurzBezeichnung;
	private String langBezeichnung;

	public Bezeichnung(String kurzBezeichnung, String langBezeichnung) {
		this.kurzBezeichnung = kurzBezeichnung;
		this.langBezeichnung = langBezeichnung;
	}

	public String getLangBezeichnung() {
		return langBezeichnung;
	}

	public String getKurzBezeichnung() {
		return kurzBezeichnung;
	}

	public boolean hasKurzBezeichnung(String kurzBezeichung) {
		return this.kurzBezeichnung.equals(kurzBezeichung);
	}
	

	@Override
	public String toString() {
		return "Bezeichnung [kurzBezeichnung=" + kurzBezeichnung
				+ ", langBezeichnung=" + langBezeichnung + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((kurzBezeichnung == null) ? 0 : kurzBezeichnung.hashCode());
		result = prime * result
				+ ((langBezeichnung == null) ? 0 : langBezeichnung.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bezeichnung other = (Bezeichnung) obj;
		if (kurzBezeichnung == null) {
			if (other.kurzBezeichnung != null)
				return false;
		} else if (!kurzBezeichnung.equals(other.kurzBezeichnung))
			return false;
		if (langBezeichnung == null) {
			if (other.langBezeichnung != null)
				return false;
		} else if (!langBezeichnung.equals(other.langBezeichnung))
			return false;
		return true;
	}
	
	
}
