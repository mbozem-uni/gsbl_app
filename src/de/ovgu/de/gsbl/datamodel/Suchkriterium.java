package de.ovgu.de.gsbl.datamodel;

/**
 * Klasse zum Suchen in der Datenbank anhand eines Begriffes und eines
 * eingegebenen Wertes
 * 
 * @author Christian
 * 
 */
public class Suchkriterium {
	
	private Begriff begriff;
	private String wert;
	
	public Suchkriterium(Begriff begriff, String wert) {
		super();
		this.begriff = begriff;
		this.wert = wert;
	}

	public String getWert() {
		return wert;
	}

	public void setWert(String wert) {
		this.wert = wert;
	}

	public Begriff getBegriff() {
		return begriff;
	}

	public void setBegriff(Begriff begriff) {
		this.begriff = begriff;
	}
	
	@Override
	public String toString() {
		return begriff.getName() + ": " + wert;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((begriff == null) ? 0 : begriff.hashCode());
		result = prime * result + ((wert == null) ? 0 : wert.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Suchkriterium other = (Suchkriterium) obj;
		if (begriff == null) {
			if (other.begriff != null)
				return false;
		} else if (!begriff.equals(other.begriff))
			return false;
		if (wert == null) {
			if (other.wert != null)
				return false;
		} else if (!wert.equals(other.wert))
			return false;
		return true;
	}
	
	

}
