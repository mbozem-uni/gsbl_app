package de.ovgu.de.gsbl;

import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.couchbase.lite.Document;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import de.ovgu.de.gsbl.settings.SettingsActivity;
import de.ovgu.de.gsbl.view.datamodel.Tab;
import de.ovgu.de.gsbl.view.datamodel.View;
import de.ovgu.de.gsbl.view.datamodel.ViewConstants;
import de.ovgu.de.gsbl.view.datamodel.ViewCreation;
import de.ovgu.de.gsbl.view.gui.io.ExportViewActivity;
import de.ovgu.de.gsbl.view.gui.io.filechooser.FileChooser;
import de.ovgu.de.gsbl.view.gui.viewcreation.CreateViewActivity;
import de.ovgu.de.gsbl.view.gui.viewcreation.EditViewActivity;
import de.ovgu.de.gsbl.view.gui.viewdata.ViewDataActivity;
import de.ovgu.de.gsbl.view.util.DataGenerator;
import de.ovgu.de.gsbl.view.util.ViewUtil;

/**
 * Activity, die für die Such- und die Ergebnis-Activity das Optionsmenü bündelt
 * 
 * @author Christian
 * 
 */
public abstract class GSBLActivity extends ActionBarActivity {

	protected static final int RESULT_SETTINGS = 1;
	protected static CopyOnWriteArrayList<de.ovgu.de.gsbl.view.datamodel.View> views;
	public static GSBLActivity reference;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		// auskommentiert, damit im Kontextmenü kein "Sicht"-Eintrag mehr
		// erscheint
		// if (itemId == R.id.viewer) {
		// initView();
		// return true;
		// } else
		if (itemId == R.id.newView) {
			createView();
			return true;
		} else if (itemId == R.id.action_loadData) {
			loadData();
			return true;
		} else if (itemId == R.id.edit_views) {
			editViews();
			return true;
		} else if (itemId == R.id.acion_importView) {
			importView();
			return true;
		} else if (itemId == R.id.action_exportView) {
			exportView();
			return true;
		} else if (itemId == R.id.action_settings) {
			startActivityForResult(new Intent(this, SettingsActivity.class),
					RESULT_SETTINGS);
			return true;
		} else if (itemId == R.id.action_help) {
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	@SuppressWarnings("deprecation")
	protected void initView(String docId) {
		if (views == null) {
			views = loadViews();
		}
		Intent intent = new Intent(this, ViewDataActivity.class);
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		StringBuilder builder = new StringBuilder();
		builder.append(sharedPrefs.getString("prefChooseView", "NULL"));
		String settings = builder.toString();
		if (settings == null || settings.isEmpty() || settings.equals("NULL")
				|| Integer.valueOf(settings) >= views.size()) {
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();

			alertDialog.setTitle("Fehlerhafte Aktion");
			alertDialog
					.setMessage("Bitte in den Einstellungen eine Standardsicht festlegen.");
			alertDialog.setIcon(R.drawable.warning);

			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			alertDialog.show();
		} else {
			de.ovgu.de.gsbl.view.datamodel.View chosenView = views.get(Integer
					.valueOf(settings));
			Application app = (Application) getApplication();
			Document doc = app.getDatabase().get(docId);
			if (doc != null) {
				Map<String, Object> dataFromDatabase = doc.getProperties();
				View tempView = ViewUtil.assignProperties(chosenView,
						dataFromDatabase);
				intent.putExtra(ViewConstants.DEFAULTVIEW.getName(), tempView);
				startActivity(intent);
			} else {
				ViewUtil.showInformation(
						"Suchergebnis nicht in Datenbank enthalten.", this);
			}
		}
	}

	/* Alte Methode, zu Testzecken noch enthalten. */
	@SuppressWarnings("deprecation")
	protected void initView( /* Document document */) {
		if (views == null) {
			views = loadViews();
		}
		Intent intent = new Intent(this, ViewDataActivity.class);
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		StringBuilder builder = new StringBuilder();
		builder.append(sharedPrefs.getString("prefChooseView", "NULL"));
		String settings = builder.toString();
		if (settings == null || settings.isEmpty() || settings.equals("NULL")
				|| Integer.valueOf(settings) >= views.size()) {
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();

			alertDialog.setTitle("Fehlerhafte Aktion");
			alertDialog
					.setMessage("Bitte in den Einstellungen eine Standardsicht festlegen.");
			alertDialog.setIcon(R.drawable.warning);

			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			alertDialog.show();
		} else {
			de.ovgu.de.gsbl.view.datamodel.View chosenView = views.get(Integer
					.valueOf(settings));
			Map<String, Object> dataFromDatabase = DataGenerator
					.initDataFromDatabaseDataModel(); // Document.getProperties()
			View tempView = ViewUtil.assignProperties(chosenView,
					dataFromDatabase);
			intent.putExtra(ViewConstants.DEFAULTVIEW.getName(), tempView);
			startActivity(intent);
		}
	}

	private void importView() {

		Intent intent = new Intent(this, FileChooser.class);
		startActivity(intent);
	}

	private void exportView() {
		Intent intent = new Intent(this, ExportViewActivity.class);
		if (views == null
				&& ViewCreation.getInstance().getCreatedViews() != null) {
			views = ViewCreation.getInstance().getCreatedViews();
		} else if (views == null
				&& ViewCreation.getInstance().getCreatedViews() == null) {
			ViewUtil.showInformation(
					"Keine Sichten zum Exportieren vorhanden.", this);
			return;
		}
		intent.putExtra(ViewConstants.VIEWS.getName(), views);
		startActivity(intent);
	}

	protected CopyOnWriteArrayList<de.ovgu.de.gsbl.view.datamodel.View> loadViews() {
		CopyOnWriteArrayList<de.ovgu.de.gsbl.view.datamodel.View> views = ViewCreation
				.getInstance().getCreatedViews();
		return views;
	}

	protected void createView() {
		Intent intent = new Intent(this, CreateViewActivity.class);
		intent.putExtra(ViewConstants.CREATION.getName(), true);
		startActivity(intent);
	}

	protected void editViews() {
		startActivity(new Intent(this, EditViewActivity.class));
	}

	protected void loadData() {
		startActivity(new Intent(this, LoadDataActivity.class));
	}

	protected void loadCustomViews() {
		reference = this;

		// Restore preferences
		SharedPreferences settings = getSharedPreferences("ViewStorage", 0);
		String storage = settings.getString("CustomViews", "");
		CopyOnWriteArrayList<View> loadedViews = new Gson().fromJson(storage,
				new TypeToken<CopyOnWriteArrayList<View>>() {
				}.getType());
		if (loadedViews != null) {
			views = new CopyOnWriteArrayList<View>();
			views = loadedViews;
			ViewCreation.getInstance().setCreatedViews(views);
		}
	}

	public static void addView(View view) {
		int i = 0;
		boolean isReplaced = false;
		if (views == null) {
			views = new CopyOnWriteArrayList<View>();
		}
		for (View v : views) {
			if (v.getName().equals(view.getName())) {
				views.set(i, v);
				isReplaced = true;
			}
			i++;
		}
		if (!isReplaced) {
			views.add(view);
		}
		ViewCreation.getInstance().setCreatedViews(views);
		GSBLActivity.reference.saveCustomViews();
	}

	public void saveCustomViews() {
		if (views != null) {
			// All objects are from android.context.Context
			SharedPreferences settings = getSharedPreferences("ViewStorage", 0);
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("CustomViews", new Gson().toJson(views).toString());
			editor.commit();
		}
	}

	public void saveView(View view) {
		// All objects are from android.context.Context
		SharedPreferences settings = getSharedPreferences("TempViewStorage", 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("View", new Gson().toJson(view).toString());
		editor.commit();
	}

	public View loadView() {
		reference = this;

		// Restore preferences
		SharedPreferences settings = getSharedPreferences("TempViewStorage", 0);
		String storage = settings.getString("View", "");
		View view = new Gson().fromJson(storage, new TypeToken<View>() {
		}.getType());
		return view;
	}

	public void saveTab(Tab tab) {
		// All objects are from android.context.Context
		SharedPreferences settings = getSharedPreferences("TabStorage", 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("SelectedTab", new Gson().toJson(tab).toString());
		editor.commit();
	}

	public Tab loadTab() {
		reference = this;

		// Restore preferences
		SharedPreferences settings = getSharedPreferences("TabStorage", 0);
		String storage = settings.getString("SelectedTab", "");
		Tab tab = new Gson().fromJson(storage, new TypeToken<Tab>() {
		}.getType());
		return tab;
	}
}