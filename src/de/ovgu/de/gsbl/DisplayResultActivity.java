package de.ovgu.de.gsbl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar.LayoutParams;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.couchbase.lite.Document;

import de.ovgu.de.gsbl.datamodel.Begriff;
import de.ovgu.de.gsbl.datamodel.Suchkriterium;
import de.ovgu.de.gsbl.view.util.Constants;

public class DisplayResultActivity extends GSBLActivity {

	private Map<String, String> searchResult; // DocIds und Name für die Anzeige
	
	private List<String> resultOrder; // DocIds in einer Reihenfolge für die Anzeige 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display_result);

		// Get the message from the intent
		List<Suchkriterium> suchkriterien = extractSuchkriterienFromIntent();

		search(suchkriterien); // füllt searchResult und resultOrder

		LinearLayout layout = (LinearLayout) findViewById(R.id.resultActivity);

		if(resultOrder.isEmpty()) {
			TextView tv = new TextView(this);
			tv.setLayoutParams(new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			tv.setText(R.string.no_search_result);
			layout.addView(tv);
		}
		
		int i = 0;
		boolean grey = false;
		for (final String docId : resultOrder) {
			TextView text = new TextView(this);
			text.setText(++i + ". " + searchResult.get(docId));
			text.setTextSize(22);
			if (grey) {
				text.setBackgroundColor(Color.LTGRAY);
				grey = false;
			} else {
				grey = true;
			}
			text.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					displayDocument(docId);
				}
			});
			layout.addView(text);

		}

	}

	/**
	 * Zeigt die Details zum Suchergebnis mit der übergebenen DocumentID in
	 * einer neuen Activity an.
	 * 
	 * @param docId
	 */
	private void displayDocument(String docId) {
		initView(docId);
	}

	/**
	 * Führt die Suche aus und speichert die Ergebnisse folgendermaßen: die Map
	 * searchResult führt zu jedem DocId den Namen auf, der in der Resultatliste
	 * angezeigt wird; die Liste resultOrder führt die docId in der
	 * Ranking-Reihenfolge auf
	 * 
	 * @param suchkriterien
	 * @return
	 */
	private void search(List<Suchkriterium> suchkriterien) {
		de.ovgu.de.gsbl.Application application = (de.ovgu.de.gsbl.Application) getApplication();
		List<Document> foundDocuments = application.getDatabase().search(
				suchkriterien);

		// Map mit DocId und Name für die Anzeige
		searchResult = new HashMap<String, String>();
		// Map mit Füllgrad und Liste der DocIds, die diesen Füllgrad aufweisen
		Map<Integer, List<String>> tempOrder = new HashMap<Integer, List<String>>();

		// geht die gefundenen Documents durch, die jeweils ein Suchergebnis
		// darstellen, und ermittelt Namen, docId und Position jedes
		// Suchergebnisses in der Rangliste
		for (Document document : foundDocuments) {
			Map<String, Object> properties = document.getProperties();
			String displayName = extractNameFromProperty(properties);
			searchResult.put(document.getId(), displayName);

			Integer fuellgrad = Integer.valueOf(properties.size());
			if (!tempOrder.containsKey(fuellgrad)) {
				tempOrder.put(fuellgrad, new ArrayList<String>());
			}
			tempOrder.get(fuellgrad).add(document.getId());
		}

		Object[] fuellgrade = tempOrder.keySet().toArray();
		Arrays.sort(fuellgrade);

		resultOrder = new ArrayList<String>();
		for (Object fuellgrad : fuellgrade) {
			resultOrder.addAll(tempOrder.get(fuellgrad));
		}

	}

	/**
	 * Sucht aus den Properties den Namen für die Anzeige heraus. Dies ist: 1.
	 * der Displayname, 2. der erste dt. RName, 3. der erste RName, 4. der erste
	 * UName; wird kein Name gefunden, wird der String
	 * "Fehler: Kein Name gefunden." zurückgegeben
	 * 
	 * @param property
	 * @return
	 */
	private String extractNameFromProperty(Map<String, Object> property) {
		String name = "Fehler: Kein Name gefunden.";
		// hinter der Property DispName verbirgt sich eine Map mit dem Eintrag
		// "Name", der den Namen beinhaltet
		if (property.containsKey(Constants.dispname)) {
			Map<String, Object> dispNameMap = (Map<String, Object>) property
					.get(Constants.dispname);
			name = (String) dispNameMap.get(Constants.name);
		} else {
			if (property.containsKey(Constants.rname)) {
				Object rnamen = property.get(Constants.rname);
				if (rnamen instanceof Map<?, ?>) {
					name = (String) ((Map<String, Object>) rnamen)
							.get(Constants.rname);
				} else {
					List<Map<String, Object>> rnamenListe = (List<Map<String, Object>>) rnamen;
					for (Map<String, Object> rnamenMap : rnamenListe) {
						if (Constants.de.equals(rnamenMap.get(Constants.spr))) {
							return (String) rnamenMap.get(Constants.rname);
						}
					}
					name = (String) rnamenListe.get(0).get(Constants.rname);
				}
			} else {
				if (property.containsKey(Constants.uname)) {
					Object rnamen = property.get(Constants.uname);
					if (rnamen instanceof Map<?, ?>) {
						name = (String) ((Map<String, Object>) rnamen)
								.get(Constants.rname);
					} else {
						List<Map<String, Object>> rnamenListe = (List<Map<String, Object>>) rnamen;
						name = (String) rnamenListe.get(0).get(Constants.rname);
					}
				}
			}
		}

		return name;
	}

	private List<Suchkriterium> extractSuchkriterienFromIntent() {
		List<Suchkriterium> suchkriterien = new ArrayList<Suchkriterium>();

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		for (String suchkriteriumAlsString : extras.keySet()) {

			// aus den übergebenen Suchkriterien müssen Objekte der Klasse
			// Suchkriterium gemacht werden, d.h. es ist eine Gruppierung nach
			// Oberbegriffen nötig
			// das Format der Suchkriterien ist: Begriff (Oberbegriff), wobei
			// Leerzeichen möglich sind

			String[] suchkriteriumAlsStringArray = suchkriteriumAlsString
					.split(" \\(");
			String begriffAlsString = suchkriteriumAlsStringArray[0];
			String oberbegriffAlsString = suchkriteriumAlsStringArray[1]
					.substring(0, suchkriteriumAlsStringArray[1].length() - 1);

			Begriff begriff = new Begriff(oberbegriffAlsString,
					new ArrayList<Begriff>());
			begriff.addUnterbegriff(new Begriff(begriffAlsString));
			Suchkriterium suchkriterium = new Suchkriterium(begriff,
					extras.getString(suchkriteriumAlsString));
			suchkriterien.add(suchkriterium);

			// zum Test können die Suchkriterien angezeigt werden
			// TextView text = new TextView(this);
			// text.setText(oberbegriffAlsString + "." + begriffAlsString);
			// layout.addView(text);
		}

		return suchkriterien;
	}
}
