package de.ovgu.de.gsbl.parser;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.Emitter;
import com.couchbase.lite.Mapper;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryEnumerator;
import com.couchbase.lite.QueryRow;
import com.couchbase.lite.Reducer;
import com.couchbase.lite.View;
import com.couchbase.lite.util.Log;

import de.ovgu.de.gsbl.datamodel.Bezeichnung;
import de.ovgu.de.gsbl.datamodel.Suchkriterium;
import de.ovgu.de.gsbl.view.util.Kurz2LangBezeichnung;
import de.ovgu.de.gsbl.view.util.Kurz2LangBezeichnungMap;

/*
 * Die Suche nach Stoffen in der Datenbank funktioniert so:
 * Der User wählt über die UI aus, welche Merkmale er durchsuchen möchte.
 * Daraufhin muss die 'setSearchableBegriffe' Methode aufgerufen werden, die dann
 * die Indizes vorbereitet zum durchsuchen der Datenbank.
 * Wenn der User anschließend über die UI eine Suche gestartet hat wird die 'search' Methode aufgerufen.
 */
public class StoffDatabase {

	private final Database db;

	private static final String TAG = "StoffDatabase";
	private final String submerkmalId = "is_submerkmal_";
	private static final String begriffsView = "de.ovgu.begriffsView";
	private static final String datenmodellView = "de.ovgu.datenmodellView";

	public StoffDatabase(Database db) {
		this.db = db;
	}

	/**
	 * Liefert das zur docId gehörende Document. Über doc.getProperties() können
	 * alle relevanten Eigenschaften des Stoffes ausgelesen werden. Liefert
	 * null, wenn kein Document mit docId existiert.
	 */
	public Document get(String docId) {
		return db.getExistingDocument(docId);
	}

	/**
	 * Liefert zu den übergebenen Suchkriterien die Einträge in der Datenbank.
	 * Dabei werden die Einträge kommplett geliefert; für die Anzeige der
	 * Suchergebnisse sind dann sehr viel weniger Informationen relevant.
	 */
	public List<Document> search(List<Suchkriterium> searchCriteria) {
		List<Document> searchResult = new ArrayList<Document>();

		Map<String, Set<String>> searchMap = new HashMap<String, Set<String>>();
		for (int i = 0; i < searchCriteria.size(); i++) {
			searchMap.put(Integer.toString(i), new HashSet<String>());
		}
		for (int currCriterionIdx = 0; currCriterionIdx < searchCriteria.size(); currCriterionIdx++) {
			Suchkriterium criterion = searchCriteria.get(currCriterionIdx);
			List<String> names = criterion.getBegriff().collectNames();
			String key = joinBegriffNames(names) + criterion.getWert();
			key = key.toUpperCase();

			Query query = db.getView(searchViewName).createQuery();
			Log.d(TAG, "Key: " + key);
			query.setStartKey(key);
			query.setEndKey(key);
			try {
				QueryEnumerator result = query.run();
				Log.d(TAG, "Result size:" + result.getCount());

				Set<String> docIds = searchMap.get(Integer
						.toString(currCriterionIdx));
				for (Iterator<QueryRow> it = result; it.hasNext();) {
					QueryRow row = it.next();
					if (!docIds.contains(row.getDocumentId())) {
						docIds.add(row.getDocumentId());
						searchResult.add(row.getDocument());
					}
				}
			} catch (CouchbaseLiteException e) {
				Log.e(TAG, "oops", e);
			}
		}

		searchResult = removeDuplicates(searchResult);
		// make sure we combine all criteria with (logical) AND
		Set<String> commonDocIds = new HashSet<String>(searchMap.get("0"));
		if (searchMap.values().size() > 1) {
			for (int i = 1; i < searchMap.values().size(); i++) {
				commonDocIds.retainAll(searchMap.get(Integer.toString(i)));
			}
		}
		for (Iterator<Document> iterator = searchResult.iterator(); iterator
				.hasNext();) {
			Document doc = iterator.next();
			if (!commonDocIds.contains(doc.getId())) {
				iterator.remove();
			}
		}

		return searchResult;
	}

	public List<Document> removeDuplicates(List<Document> l) {
		Set<Document> s = new TreeSet<Document>(new Comparator<Document>() {
			@Override
			public int compare(Document o1, Document o2) {
				if (o1.getId().equals(o2.getId())) {
					return 0;
				}
				return 1;
			}
		});
		s.addAll(l);

		List<Document> filtered = new ArrayList<Document>(s);
		return filtered;
	}

	private static final String searchViewName = "de.ovgu.SearchView";

	public void setSearchableBegriffe(
			final List<de.ovgu.de.gsbl.datamodel.Begriff> selectedSearch) {
		final List<List<String>> begriffsList = extractUnterbegriffe(selectedSearch);


		db.getView(searchViewName).delete();
		View searchView = db.getView(searchViewName);
		searchView.setMap(new Mapper() {
			@Override
			public void map(Map<String, Object> document, Emitter emitter) {
				for (List<String> begriffsKette : begriffsList) {
					Set<String> values = getValuesFromDoc(document,
							begriffsKette, new HashSet<String>());
					for (String value : values) {
						String joined = (joinBegriffNames(begriffsKette) + value).toUpperCase();
						emitter.emit(joined, null);
					}
				}
			}
		}, "2");
	}

	public Kurz2LangBezeichnungMap getKurz2LangBezeichnungMap()
			throws CouchbaseLiteException {
		View dmView = db.getView(datenmodellView);
		dmView.setMap(new Mapper() {
			@Override
			public void map(Map<String, Object> document, Emitter emitter) {
				if (document.get(StoffDatenParser.docType) != null
						&& document.get(StoffDatenParser.docType).equals(
								StoffDatenParser.docTypeDatenmodell)) {
					emitter.emit("sth", null);
				}
			}
		}, "2");

		Query query = db.getView(datenmodellView).createQuery();
		QueryEnumerator result = query.run();
		for (Iterator<QueryRow> it = result; it.hasNext();) {
			QueryRow row = it.next();

			Kurz2LangBezeichnungMap k2lMap = new Kurz2LangBezeichnungMap();
			Map<String, Object> bezMap = (Map<String, Object>) row
					.getDocument().getProperty(
							StoffDatenParser.docTypeDatenmodell);
			toK2LMap(k2lMap, bezMap);

			return k2lMap;
		}

		return new Kurz2LangBezeichnungMap();
	}

	private Bezeichnung bezStringToBez(String s) {
		String[] parts = s.split(StoffDatenParser.bezeichnungsSplit);
		return new Bezeichnung(parts[0], parts[1]);
	}

	private void toK2LMap(Kurz2LangBezeichnungMap k2lMap,
			Map<String, Object> bezMap) {
		for (Entry<String, Object> entry : bezMap.entrySet()) {
			Kurz2LangBezeichnungMap newMap = new Kurz2LangBezeichnungMap();
			k2lMap.put(bezStringToBez(entry.getKey()), newMap);
			if (entry.getValue() != null) {
				toK2LMap(newMap, (Map<String, Object>) entry.getValue());
			}
		}
	}

	private String joinBegriffNames(List<String> b) {
		String n = "";
		for (String bb : b)
			n += bb + ".";

		return n;
	}

	private Set<String> getValuesFromDoc(Map<String, Object> doc,
			List<String> begriffsKette, Set<String> acc) {
		if (begriffsKette.size() >= 1) {
			Object o = doc.get(begriffsKette.get(0));
			if (o != null) {
				if (o instanceof String) {
					acc.add((String) o);
				} else if (o instanceof List) {
					List<Object> values = (List<Object>) o;
					if (!values.isEmpty()) {
						Object liElem = values.get(0);
						if (liElem instanceof String) {
							acc.addAll((List<String>) o);
						} else if (liElem instanceof Map) {
							List<Map<String, Object>> multiple = (List<Map<String, Object>>) o;
							for (Map<String, Object> single : multiple) {
								Set<String> singleValues = getValuesFromDoc(
										single,
										begriffsKette.subList(1,
												begriffsKette.size()),
										new HashSet<String>());
								if (singleValues != null
										&& !singleValues.isEmpty()) {
									acc.addAll(singleValues);
								}
							}
						}
					}
				} else if (o instanceof Map) {
					Map<String, Object> oMap = (Map<String, Object>) o;
					Set<String> oMapValues = getValuesFromDoc(oMap,
							begriffsKette.subList(1, begriffsKette.size()),
							new HashSet<String>());
					if (oMapValues != null && !oMapValues.isEmpty()) {
						acc.addAll(oMapValues);
					}
				}
			}
		}

		return acc;
	}

	private List<List<String>> extractUnterbegriffe(
			List<de.ovgu.de.gsbl.datamodel.Begriff> begriffe) {
		List<List<String>> begriffsList = new ArrayList<List<String>>();
		for (de.ovgu.de.gsbl.datamodel.Begriff begriff : begriffe) {
			extractHelper(begriff, new ArrayList<String>(), begriffsList);
		}
		return begriffsList;
	}

	private void extractHelper(de.ovgu.de.gsbl.datamodel.Begriff begriff,
			List<String> acc, List<List<String>> begriffsList) {
		acc.add(begriff.getName());
		if (begriff.getUnterbegriffe().isEmpty()) {
			begriffsList.add(acc);
		} else {
			for (de.ovgu.de.gsbl.datamodel.Begriff unterBegriff : begriff
					.getUnterbegriffe()) {
				List<String> accCopy = new ArrayList<String>(acc);
				extractHelper(unterBegriff, accCopy, begriffsList);
			}
		}
	}

	public List<de.ovgu.de.gsbl.datamodel.Begriff> getBegriffe() {

//		List<de.ovgu.de.gsbl.datamodel.Begriff> begriffeOld = getBegriffeOld();
//		System.out.println(begriffeOld);

		try {
			Kurz2LangBezeichnungMap k2lMap = getKurz2LangBezeichnungMap();
			Kurz2LangBezeichnung kurz2LangBezeichnung = new Kurz2LangBezeichnung(
					k2lMap);
			List<de.ovgu.de.gsbl.datamodel.Begriff> result = kurz2LangBezeichnung
					.getBegriffe();
			System.out.println(result);
			return result;

		} catch (CouchbaseLiteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ArrayList<de.ovgu.de.gsbl.datamodel.Begriff>();
		}
	}

	private List<de.ovgu.de.gsbl.datamodel.Begriff> getBegriffeOld() {
		View attrView = db.getView(begriffsView);
		attrView.setMapReduce(new Mapper() {
			@Override
			public void map(Map<String, Object> document, Emitter emitter) {
				try {
					mapBegriffe(document, new ArrayList<String>(), emitter);
				} catch (Exception e) {
					Log.e(TAG, "Oops", e);
				}

			}
		}, new Reducer() {
			@Override
			public Object reduce(List<Object> keys, List<Object> values,
					boolean rereduce) {
				Log.d(TAG, "Reducing. Keys: " + keys + "\n Values: " + values);
				Map<String, Begriff> begriffsMap = new HashMap<String, Begriff>();
				for (int i = 0; i < keys.size(); i++) {
					String curr = (String) keys.get(i);
					if (curr.contains(submerkmalId)) {
						Log.d(TAG, "Curr is submerkmal: " + curr);
						// derzeit werden submerkmale einfach ignoriert
						continue;
					}

					Begriff currBegriff = begriffsMap.get(curr);
					if (currBegriff == null) {
						currBegriff = new Begriff(curr);
						begriffsMap.put(curr, currBegriff);
					}

					List<String> currFields = (List<String>) values.get(i);
					for (String f : currFields) {
						currBegriff.addField(f);
					}

				}
				List<de.ovgu.de.gsbl.datamodel.Begriff> begriffe = mapToViewModel(begriffsMap);
				Log.d(TAG, "Reduced to: " + begriffe);

				return begriffe;
			}

			private List<de.ovgu.de.gsbl.datamodel.Begriff> mapToViewModel(
					Map<String, Begriff> begriffsMap) {
				List<de.ovgu.de.gsbl.datamodel.Begriff> begriffe = new ArrayList<de.ovgu.de.gsbl.datamodel.Begriff>();
				for (Begriff original : begriffsMap.values()) {
					de.ovgu.de.gsbl.datamodel.Begriff mapped = new de.ovgu.de.gsbl.datamodel.Begriff(
							original.name);
					for (String field : original.fields) {
						de.ovgu.de.gsbl.datamodel.Begriff sub = new de.ovgu.de.gsbl.datamodel.Begriff(
								field);
						mapped.addUnterbegriff(sub);
					}
					begriffe.add(mapped);
				}
				return begriffe;
			}
		}, "2");

		Query query = db.getView(begriffsView).createQuery();
		QueryEnumerator result;
		try {
			result = query.run();
			for (Iterator<QueryRow> it = result; it.hasNext();) {
				QueryRow row = it.next();
				return (List<de.ovgu.de.gsbl.datamodel.Begriff>) row.getValue();
			}
		} catch (CouchbaseLiteException e) {
			Log.e(TAG, "", e);
		}

		Log.d(TAG, "Found 0 results.");
		return new ArrayList<de.ovgu.de.gsbl.datamodel.Begriff>();

	}

	private void mapBegriffe(Map<String, Object> current,
			List<String> oberbegriffe, Emitter emitter) throws Exception {
		List<String> fields = new ArrayList<String>();

		for (Map.Entry<String, Object> curr : current.entrySet()) {

			List<String> newOberbegriffe = new ArrayList<String>(oberbegriffe);
			newOberbegriffe.add(curr.getKey());

			if (curr.getValue() instanceof String) {
				fields.add(curr.getKey());
			} else if (curr.getValue() instanceof Map) {
				mapBegriffe((Map<String, Object>) curr.getValue(),
						newOberbegriffe, emitter);
			} else if (curr.getValue() instanceof List) {
				List<Object> currList = (List<Object>) curr.getValue();
				if (!currList.isEmpty()) {
					Object liElem = currList.get(0);
					if (liElem instanceof String) {
						fields.add(curr.getKey());
					} else if (liElem instanceof Map) {
						for (Object o : currList) {
							Map<String, Object> currMap = (Map<String, Object>) o;
							mapBegriffe(currMap, newOberbegriffe, emitter);
						}
					}
				}
			} else {
				throw new Exception("Current Value has unhandled type: "
						+ curr.getClass());
			}

		}

		if (oberbegriffe.isEmpty()) {
			Log.d(TAG,
					"Not emitting anything since oberbegriffe is empty. Fields are: "
							+ fields);
		} else {
			String key = "";
			for (String o : oberbegriffe)
				key = key + o + ".";
			key = key.substring(0, key.length() - 1);
			if (oberbegriffe.size() > 1) {
				key = submerkmalId + key;
			}

			if (fields.isEmpty()) {
				Log.d(TAG, "Not emitting Key: " + key
						+ " because field list is empty.");
			} else {
				Log.d(TAG, "Emitting Key: " + key + " with fields: " + fields);
				emitter.emit(key, fields);
			}
		}

	}

	public static class Begriff {
		public final String name;
		private final Set<String> fields = new HashSet<String>();

		public Begriff(String name) {
			this.name = name;
		}

		public void addField(String s) {
			this.fields.add(s);
		}

		public Set<String> getFields() {
			return new HashSet<String>(fields);
		}

		@Override
		public String toString() {
			return "Begriff [name=" + name + ", fields=" + fields + "]";
		}

	}

	/*
	 * testmethode zum querien private void queryUname() throws
	 * CouchbaseLiteException { View unameView = db.getView("uname");
	 * 
	 * final String testTag = "ExampleQuery"; unameView.setMapReduce(new
	 * Mapper() {
	 * 
	 * @Override public void map(Map<String, Object> document, Emitter emitter)
	 * { if (document.containsKey("UNAME")) { Object unameObj =
	 * document.get("UNAME"); if (unameObj instanceof Map) { Map<String, Object>
	 * uname = (Map<String, Object>) document.get("UNAME"); if
	 * (uname.containsKey("RNAME") && uname.get("RNAME") instanceof String) {
	 * String rname = (String) uname.get("RNAME"); Log.d(testTag, "Emitting: " +
	 * rname); emitter.emit(rname, null); }
	 * 
	 * } else if (unameObj instanceof List) { List<Map<String, Object>> unames
	 * =(List<Map<String, Object>>) document.get("UNAME"); //for
	 * (Map.Entry<String, Object> entry : unames.) for (Map<String, Object>
	 * uname : unames) { if (uname.containsKey("RNAME") && uname.get("RNAME")
	 * instanceof String) { String rname = (String) uname.get("RNAME");
	 * Log.d(testTag, "Emitting: " + rname); emitter.emit(rname, null); } } } }
	 * 
	 * } }, new Reducer() {
	 * 
	 * @Override public Object reduce(List<Object> keys, List<Object> values,
	 * boolean rereduce) { Log.d(testTag, "Reducing. Keys: " + keys +
	 * "\n Values: " + values); return null; } }, "1");
	 * 
	 * Query query = db.getView("uname").createQuery(); List<Object> keys = new
	 * ArrayList<Object>(); keys.add("BENZOL"); query.setKeys(keys);
	 * QueryEnumerator result = query.run();
	 * 
	 * 
	 * Log.d(testTag, "Found " + result.getCount() + " results."); for
	 * (Iterator<QueryRow> it = result; it.hasNext(); ) { QueryRow row =
	 * it.next(); Log.d(testTag, "----------------------------------");
	 * Log.d(testTag, "DocID: " + row.getDocumentId()); } }
	 */
}
