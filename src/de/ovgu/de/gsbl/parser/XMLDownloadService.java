package de.ovgu.de.gsbl.parser;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import com.couchbase.lite.Database;

import de.ovgu.de.gsbl.parser.StoffDatenParser.ParserProgressListener;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class XMLDownloadService extends IntentService implements ParserProgressListener {
	
	public static final String URL = "de.ovgu.gsbl.parser.XMLDownloadService.url";
	public static final String xmlDownloadServiceProgress = "de.ovgu.gsbl.parser.XMLDownloadService.Progress";
	public static final String xmlDownloadServiceProgressCount = "de.ovgu.gsbl.parser.XMLDownloadService.Progress.count";
	public static final String xmlDownloadServiceFinished = "de.ovgu.gsbl.parser.XMLDownloadService.Finished";
	private static final String TAG = "xmlDownloadService";

	private int parsedCount = 0;
	
	public XMLDownloadService() {
		super("XmlDownloadService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Log.d(TAG, "onHandleIntent");
		
		String xmlUrl = intent.getStringExtra(URL);
		try {
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(false);
			XmlPullParser xpp = factory.newPullParser();
			xpp.setInput(new InputStreamReader(getUrlData(xmlUrl)));

			de.ovgu.de.gsbl.Application application = (de.ovgu.de.gsbl.Application) getApplication();
			Database db = application.getRawDatabase();
			
			StoffDatenParser stoffDatenParser = new StoffDatenParser(xpp, db);
			
			parsedCount = 0;
			stoffDatenParser.addProgressListener(this);		
			stoffDatenParser.process();
			stoffDatenParser.removeProgressListener(this);
			
			Intent broadcastIntent = new Intent(xmlDownloadServiceProgress);
			broadcastIntent.putExtra(xmlDownloadServiceFinished, true);
			LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
		} catch (Exception e) {
			Log.e(TAG, "oops", e);
		}
	}
	
	
	@Override
	public void stoffHasBeenParsed() {
		parsedCount++;
		if (parsedCount % 5 == 0) {
			Log.d(TAG, "publishing update after " + parsedCount + " stoffe have been parsed.");
			
			Intent intent = new Intent(xmlDownloadServiceProgress);
			intent.putExtra(xmlDownloadServiceProgressCount, parsedCount);
			LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
		}
	}
	
	private InputStream getUrlData(String url) {
		try {
			DefaultHttpClient client = new DefaultHttpClient();
			HttpGet method = new HttpGet(new URI(url));
			HttpResponse res = client.execute(method);
			return res.getEntity().getContent();
		} catch (Exception e) {
			Log.e(TAG, "oops", e);
		}
		return null;
	}
	

	
}
