package de.ovgu.de.gsbl.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.xmlpull.v1.XmlPullParser;

import android.util.Log;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryEnumerator;
import com.couchbase.lite.QueryRow;

import de.ovgu.de.gsbl.datamodel.Bezeichnung;
import de.ovgu.de.gsbl.view.util.Kurz2LangBezeichnungMap;

public class StoffDatenParser {
	
	public static final String docType = "docType";
	public static final String docTypeStoff = "stoff";
	
	public static final String docTypeDatenmodell = "datenmodell";
	
	private final XmlPullParser xpp;
	private final Database database;
	
	final static String TAG = "StoffDatenParser";
	
	private final List<ParserProgressListener> progressListeners = new ArrayList<ParserProgressListener>();
	public void addProgressListener(ParserProgressListener listener) {
		this.progressListeners.add(listener);
		Log.d(TAG, "weve added a listener: " + progressListeners.size());
	}
	public void removeProgressListener(ParserProgressListener listener) {
		this.progressListeners.remove(listener);
		Log.d(TAG, "weve removed a listener: " + progressListeners.size());
	}
	private void notifyProgressListeners() {
		for (ParserProgressListener listener : progressListeners) {
			listener.stoffHasBeenParsed();
		}
	}
		
	public StoffDatenParser(XmlPullParser parser, Database database) {
		this.xpp = parser;
		this.database = database;
	}
	
	public void process() throws Exception {
		clearDatabase();
		startReading();
	}
	
	private void clearDatabase() {
		try {
			Query query = database.createAllDocumentsQuery();
			QueryEnumerator result = query.run();
			for (Iterator<QueryRow> it = result; it.hasNext(); ) {
			    QueryRow row = it.next();
			    Document doc = row.getDocument();
			    doc.purge();
			    
			    /*
			    //deleting only sets a flag on the document and doesn't remove it from the database
			    //resulting in large databases 
			    //so we use doc.purge(); instead
			    if (doc.getProperty(docType) != null && doc.getProperty(docType).equals(docTypeStoff)) {
			    	row.getDocument().delete();
			    }
			    */
			}	
		} catch (CouchbaseLiteException e) {
			Log.e(TAG, "error in clearDatabase", e);
		}
	}

    private void startReading() throws Exception {
        xpp.next();
        Log.d(TAG, "EventType in startReading: " + xpp.getEventType());
        while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) {
            if (xpp.getEventType() == XmlPullParser.START_TAG && xpp.getName().equalsIgnoreCase("stoff")) {
                readStoff(xpp);
            } else if (xpp.getEventType() == XmlPullParser.START_TAG && xpp.getName().equalsIgnoreCase("datenmodell")) {
            	//readDatenmodell(xpp);
            	readDatenmodellCategory(xpp);
            }
            xpp.next();
        }
    }
    
    private void readDatenmodell(XmlPullParser xpp) throws Exception {
    	int startingDepth = xpp.getDepth();
    	while (true) {
    		//xpp.next();
    		readDatenmodellCategory(xpp);
    		if (xpp.getDepth() == startingDepth) {
    			break;
    		}
    		
    		
    	}
    }
    
    public static final String bezeichnungsSplit = "---";
    private void readDatenmodellCategory(XmlPullParser xpp) throws Exception {
    	Map<String, Object> bezeichnungsMap = new HashMap<String, Object>();
    	/*
		Map(
			Bez("AL", "IDENTMERKMALE") -> Map(
				Bez("ALGM", "Allgemeine Merkmale") -> Map(
					Bez("DISPNAME", "Displayname") -> Map(
						Bez("NAME", "Name") -> null, //or empty Map
						Bez("SPR", "Sprachkennung") -> null //or empty Map
					)
				)
			)
		)
    	 */
    	
    	int startingDepth = xpp.getDepth();
    	int prevDepth = -1;
    	Map<String, Object> currMap = bezeichnungsMap;
    	final Map<String, Object> origMap = bezeichnungsMap;
    	List<String> bezeichnungsList = new ArrayList<String>();
    	while (true) {
    		xpp.next();
    		
    		final int currDepth = xpp.getDepth();
    		if (currDepth == startingDepth) {
    			break;
    		}
    		if (xpp.getEventType() == XmlPullParser.END_TAG) {
    			continue;
    		}
    		
    		if (currDepth < prevDepth) {
    			int dropAmount = prevDepth - currDepth + 1;
    			dropFromList(bezeichnungsList, dropAmount);
    			
    			currMap = origMap;
    			for (int i = 0; i < bezeichnungsList.size(); i++) {
    				currMap = (Map<String, Object>) currMap.get(bezeichnungsList.get(i));
    			}
    			
    			final Bezeichnung bez = readBezeichnung(xpp);   
    			currMap = addBezeichnung(bez, currMap, bezeichnungsList);
    			
    			prevDepth = currDepth;
    			
    		} else if (currDepth == prevDepth) {
    			dropFromList(bezeichnungsList, 1);
    			
    			currMap = origMap;
    			for (int i = 0; i < bezeichnungsList.size(); i++) {
    				currMap = (Map<String, Object>) currMap.get(bezeichnungsList.get(i));
    			}
    			
    			final Bezeichnung bez = readBezeichnung(xpp);
    			currMap = addBezeichnung(bez, currMap, bezeichnungsList);
    			
    		} else { //currDepth > prevDepth
    			final Bezeichnung bez = readBezeichnung(xpp);
    			currMap = addBezeichnung(bez, currMap, bezeichnungsList);
    			
    			prevDepth = currDepth;
    		}
    		
    	}
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(docType, docTypeDatenmodell);
        properties.put(docTypeDatenmodell, bezeichnungsMap);
        
        Document document = database.createDocument();
        document.putProperties(properties);
    }
    
    private Map<String, Object> addBezeichnung(Bezeichnung bez, Map<String, Object> currMap, List<String> bezeichnungsList) {
		final Map<String, Object> newMap = new HashMap<String, Object>();

		currMap.put(bez.getKurzBezeichnung()+bezeichnungsSplit+bez.getLangBezeichnung(), newMap);		
		bezeichnungsList.add(bez.getKurzBezeichnung()+bezeichnungsSplit+bez.getLangBezeichnung());
		
		return newMap;
    }
    
	private void printMap(Map<Bezeichnung, Object> map) {
		for (Entry<Bezeichnung, Object> entry : map.entrySet()) {
			Log.d("testing", String.format("%s", entry.getKey()));
			printMap((Map<Bezeichnung, Object>) entry.getValue());
		}
	}
	
	
    private Bezeichnung readBezeichnung(XmlPullParser xpp) {
		final String kurzBez = xpp.getName();
		final String langBez = xpp.getAttributeValue(null, "LBZ");
		return new Bezeichnung(kurzBez, langBez);
    }
    
    private void dropFromList(List<?> list, int amount) {
    	for (int i = 0; i < amount; i++) {
    		//if (!list.isEmpty()) {
    			list.remove(list.size()-1);
	    	//}    		
		}
    }

    private void readStoff(XmlPullParser xpp) throws Exception {
        Map<String, Object> values = new HashMap<String, Object>();
        values.put(docType, docTypeStoff);

        int startingDepth = xpp.getDepth();
        List<String> nestedTerms = new ArrayList<String>();
        boolean done = false;
        while (!done) {
        	xpp.next();
        	
            int eventType = xpp.getEventType();if(eventType == XmlPullParser.TEXT) {
           	 if (xpp.getText().trim().length() == 0) {
           		 continue;
           	 }
            }       	
        	
            int currDepth = xpp.getDepth();
            
            if (currDepth == startingDepth) {
                done = true;
            } else {
                if(xpp.getEventType() == XmlPullParser.START_TAG) {
                	
                	Map<String, Object> currMap = values;
                	for (int i = 0; i < nestedTerms.size(); i++) {                		
                        Object curr = currMap.get(nestedTerms.get(i));
                        if (curr instanceof Map) {
                        	currMap = (Map<String, Object>) curr;
                        } else {
                        	List<Map<String, Object>> currList = (List<Map<String,Object>>) curr;
                        	currMap = currList.get(currList.size()-1);
                        }
                	}
                	
                	/*
                	Log.d (TAG, "----------------------------------");
                	Log.d (TAG, "Start Tag found: " + xpp.getName());
                	Log.d (TAG, "Values are: " + values);
                	Log.d (TAG, "CurrMap is: " + currMap);
                	Log.d (TAG, "NestedTerms: " + nestedTerms);
                	*/
                	//hier entscheiden, ob eine neue map da reingepackt werden muss, oder ob die existierende map in eine liste geändert werden muss                	
                	boolean tagIsDone = false;
                	if (currMap.get(xpp.getName()) != null) {                		
                		Object existing = currMap.get(xpp.getName());
                		if (existing instanceof Map) {
                    		Map<String, Object> existingMap = (Map<String, Object>) currMap.get(xpp.getName());
                    		if (existingMap.keySet().size() != 0) {
                    			//Log.d (TAG, "Found multiple occurences for the first time for " + xpp.getName());
                    			//Log.d (TAG, "ExistingMap: " + existingMap);
                    			tagIsDone = true;
                    			List<Map<String, Object>> valueList = new ArrayList<Map<String,Object>>();
                    			valueList.add(existingMap);
                    			valueList.add(new HashMap<String, Object>());
                    			currMap.put(xpp.getName(), valueList);
                    		}
                		} else if (existing instanceof String) {
                			//Log.d (TAG, "Found string multiple times" );
                			tagIsDone = true;
                			String existingString = (String) existing;
                			List<String> multiples = new ArrayList<String>();
                			multiples.add(existingString);
                			currMap.put(xpp.getName(), multiples);
                		} else {
            				tagIsDone = true;
            				List<Object> existingList2 = (List<Object>) existing;
            				if (existingList2.isEmpty() || !(existingList2.get(0) instanceof String)) {
            					existingList2.add(new HashMap<String, Object>());
            				}                			
                		}
                		
                	}
                	if (!tagIsDone) {
                		currMap.put(xpp.getName(), new HashMap<String, Object>());
                	}
                	nestedTerms.add(nestedTerms.size(), xpp.getName());
                    
                } else if (xpp.getEventType() == XmlPullParser.TEXT) {
                	/*
                	Log.d (TAG, "----------------------------------");
                	Log.d (TAG, "Reading Text: " + xpp.getText());
                	Log.d (TAG, "NestedTerms: " + nestedTerms);
                	Log.d (TAG, "Values are: " + values);
                	*/
                	
                    Map<String, Object> currMap;                    
                    Object curr = values.get(nestedTerms.get(0));
                    if (curr instanceof Map) {
                    	currMap = (Map<String, Object>) curr;
                    } else {
                    	List<Map<String, Object>> currList = (List<Map<String,Object>>) curr;
                    	currMap = currList.get(currList.size()-1);
                    }
                    for (int i = 1; i <= nestedTerms.size() - 2; i++) {
                        curr = currMap.get(nestedTerms.get(i));
                        if (curr instanceof Map) {
                        	currMap = (Map<String, Object>) curr;
                        } else {
                        	List<Map<String, Object>> currList = (List<Map<String,Object>>) curr;
                        	currMap = currList.get(currList.size()-1);
                        }
                    }
                    
                    //Log.d (TAG, "CurrMap is: " + currMap);
                    
                    Object existing = currMap.get(nestedTerms.get(nestedTerms.size()-1));
                    if (existing != null && existing instanceof List) {
                		List<String> currStrList = (List<String>) existing;
                		currStrList.add(xpp.getText());
                    } else {
                    	currMap.put(nestedTerms.get(nestedTerms.size()-1).toUpperCase(), xpp.getText());
                    }
                    
                } else if (xpp.getEventType() == XmlPullParser.END_TAG) {
                    nestedTerms.remove(nestedTerms.size()-1);
                } else {
                    throw new Exception("Unhandled Tag encountered! EventType: " + xpp.getEventType());
                }

            }
        }

        Log.d (TAG, "ReadStoff Results");
        
        Document document = database.createDocument();
        document.putProperties(values);
        notifyProgressListeners();
    }
    
    public interface ParserProgressListener {
    	void stoffHasBeenParsed();
    }
	
}
