package de.ovgu.de.gsbl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar.LayoutParams;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.couchbase.lite.CouchbaseLiteException;

import de.ovgu.de.gsbl.view.util.Kurz2LangBezeichnung;
import de.ovgu.de.gsbl.view.util.Kurz2LangInstance;

public class MainActivity extends GSBLActivity {

	protected static boolean isLoaded = false;

	private List<String> selectedSearchItemsKurzbezeichnung;

	private List<String> selectedSearchItemsLangbezeichnung;

	private static final String user_input = "user_search_input";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initalizeLayout();
		loadCustomViews();

		// initialisiere k2lBezeichnung-Map and -Wrapper
		try {
			Kurz2LangBezeichnung k2lBezeichnung = new Kurz2LangBezeichnung(
					((Application) getApplication()).getDatabase()
							.getKurz2LangBezeichnungMap());
			Kurz2LangInstance.getInstance().setKurz2LangBezeichnung(
					k2lBezeichnung);
		} catch (CouchbaseLiteException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		initalizeLayout();
		loadCustomViews();
	}

	@Override
	protected void onStop() {
		super.onStop();
		saveCustomViews();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	private void initalizeLayout() {
		setContentView(R.layout.activity_main);
		LinearLayout layout = (LinearLayout) findViewById(R.id.mainActivity);

		// auskommentiert, damit keine Standard-Sicht mehr geladen wird
		// Lade vordefinierte Sichten
		// if (!isLoaded) {
		// ViewCreation.getInstance().addView(
		// DataGenerator.initTestView().copy());
		// isLoaded = true;
		// }

		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

		this.clearSelectedSearchItems();
		List<String> items = getSelectedSearchItemsLangbezeichnung();
		if (items.isEmpty()) {
			TextView tv = new TextView(this);
			tv.setLayoutParams(params);
			tv.setText(R.string.choose_search_items);
			layout.addView(tv);
		} else {
			int itemNumber = 0;

			for (String item : items) {

				TextView textView = new TextView(this);
				textView.setLayoutParams(params);
				textView.setText(item);
				// textView.setId(++itemNumber);
				layout.addView(textView);

				EditText editText = new EditText(this);
				editText.setId(itemNumber++);
				editText.setLayoutParams(params);
				layout.addView(editText);

			}

			// hier werden die beiden Buttons hinzugefügt
			LinearLayout buttonGroup = new LinearLayout(this);
			layout.addView(buttonGroup);

			Button button = new Button(this);
			button.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					clear(view);
				}
			});

			button.setText(getString(R.string.button_clear));
			buttonGroup.addView(button);

			button = new Button(this);
			button.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					search(view);
				}
			});

			button.setText(getString(R.string.button_search));
			buttonGroup.addView(button);

		}
	}

	private void initializeSelectedSearchItems() {
		selectedSearchItemsKurzbezeichnung = new ArrayList<String>();
		selectedSearchItemsLangbezeichnung = new ArrayList<String>();

		// die SearchItems entsprechen den ausgewaehlten Preferences
		SharedPreferences prefManager = PreferenceManager
				.getDefaultSharedPreferences(this);

		try {
			Application thisApp = (Application) getApplication();
			Kurz2LangBezeichnung k2lBezeichnung = new Kurz2LangBezeichnung(
					thisApp.getDatabase().getKurz2LangBezeichnungMap());

			Map<String, ?> prefs = prefManager.getAll();
			for (String pref : prefs.keySet()) {
				if (pref.startsWith("prefChooseSearchItems")
						&& (Boolean) prefs.get(pref)) {
					// Die Preferences für die Suche werden nach dem Muster
					// prefChooseSearchItems.Merkmal.Feld gespeichert, wobei
					// dort die Kurzbezeichnung verwendet wird
					String[] prefNameParts = pref.split("\\.");
					String merkmalsName = k2lBezeichnung
							.getLangBezeichnung(prefNameParts[1]);
					String feldName = k2lBezeichnung.getLangBezeichnung(
							prefNameParts[1], prefNameParts[2]);
					selectedSearchItemsLangbezeichnung.add(feldName + " ("
							+ merkmalsName + ")");
					selectedSearchItemsKurzbezeichnung.add(prefNameParts[2]
							+ " (" + prefNameParts[1] + ")");
					;
				}
			}

		} catch (CouchbaseLiteException e) {
			e.printStackTrace();
			throw new RuntimeException(e.toString());
		}
	}

	/**
	 * Setzt die gecashten Suchkriterien in den Objektvariablen
	 * selectedSearchItemsKurzbezeichnung und selectedSearchItemsLangbezeichnung
	 * zurück. Ist u.a. für das Initialisieren des Layouts notwendig.
	 */
	private void clearSelectedSearchItems() {
		this.selectedSearchItemsKurzbezeichnung = null;
		this.selectedSearchItemsLangbezeichnung = null;
	}

	private List<String> getSelectedSearchItemsLangbezeichnung() {
		if (selectedSearchItemsLangbezeichnung == null
				|| selectedSearchItemsLangbezeichnung.isEmpty()) {
			initializeSelectedSearchItems();
		}

		return this.selectedSearchItemsLangbezeichnung;
	}

	private List<String> getSelectedSearchItemsKurzbezeichnung() {
		if (selectedSearchItemsKurzbezeichnung == null
				|| selectedSearchItemsKurzbezeichnung.isEmpty()) {
			initializeSelectedSearchItems();
		}

		return this.selectedSearchItemsKurzbezeichnung;
	}

	private void clear(View view) {
		LinearLayout layout = (LinearLayout) findViewById(R.id.mainActivity);
		for (int i = 0; i < layout.getChildCount(); i++) {
			View child = layout.getChildAt(i);
			if (child instanceof EditText) {
				((EditText) child).setText("");
			}
		}
	}

	private void search(View view) {

		Intent intent = new Intent(this, DisplayResultActivity.class);

		List<String> searchItems = getSelectedSearchItemsKurzbezeichnung();
		for (int itemNumber = 0; itemNumber < searchItems.size(); itemNumber++) {
			EditText editText = (EditText) findViewById(itemNumber);
			if (editText.getText() != null
					&& editText.getText().toString() != null
					&& !editText.getText().toString().equals("")) {
				String message = editText.getText().toString();
				intent.putExtra(searchItems.get(itemNumber), message);
			}
		}
		startActivity(intent);

	}

}
