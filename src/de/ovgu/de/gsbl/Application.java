package de.ovgu.de.gsbl;

import java.io.IOException;

import android.util.Log;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Manager;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryEnumerator;
import com.couchbase.lite.android.AndroidContext;

import de.ovgu.de.gsbl.parser.StoffDatabase;
import de.ovgu.de.gsbl.parser.StoffDatenParser;

public class Application extends android.app.Application {

	private Manager manager;
	private Database database;
	private StoffDatabase stoffDatabase;

	private static final boolean useLocalXmlFile = true;

	private static final String tag = "gsbl.application";
	private static final String dbName = "de.ovgu.gsbl.database";

	@Override
	public void onCreate() {
		super.onCreate();

		initDatabase();
	}

	public StoffDatabase getDatabase() {
		return stoffDatabase;
	}

	public Database getRawDatabase() {
		return database;
	}

	private void initDatabase() {
		try {
			manager = new Manager(new AndroidContext(getApplicationContext()),
					Manager.DEFAULT_OPTIONS);
		} catch (IOException e) {
			Log.e(tag, "error in initDatabase", e);
			return;
		}

		try {
			database = manager.getDatabase(dbName);
		} catch (CouchbaseLiteException e) {
			Log.e(tag, "error in initDatabase", e);
			return;
		}

		StoffDatenParser parser;
		if (useLocalXmlFile) {
			parser = new StoffDatenParser(getApplicationContext()
					.getResources().getXml(R.xml.stoff_daten), database);
		} else {
			// Todo:
			// download xml from url to init database
		}

		if (parser != null && database != null) {
			try {
				boolean databaseIsEmpty = database.createAllDocumentsQuery()
						.run().getCount() == 0;
				if (databaseIsEmpty) {
					Log.d(tag, "Starting to load XML data into the database.");
					parser.process();
				} else {
					Log.d(tag,
							"Not loading XML data since the database is not empty.");
				}
				Log.d(tag, "Finished loading XML data.");
			} catch (Exception e) {
				Log.e(tag, "error in initDatabase", e);
				return;
			}
		}
		stoffDatabase = new StoffDatabase(database);
	}
}
