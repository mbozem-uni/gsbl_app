package de.ovgu.de.gsbl.view.gui.viewdata;

import java.util.List;
import java.util.Map;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import de.ovgu.de.gsbl.R;

public class PropertiesFragment extends Fragment {

	public final static String ARG_PROPERTIES_NUMBER = "properties_number";

	public PropertiesFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_properties,
				container, false);

		int i = getArguments().getInt(ARG_PROPERTIES_NUMBER);
		String property = ViewDataActivity.mTabs[i];

		ExpandableListView expListView = (ExpandableListView) rootView
				.findViewById(R.id.expandableListView1);

		List<String> listDataHeader = ViewDataActivity.dataHeader[i];
		Map<String, List<String>> listDataChild = ViewDataActivity.dataChildren[i];

		ListViewDataAdapter listAdapter = new ListViewDataAdapter(
				getActivity(), listDataHeader, listDataChild);

		expListView.setAdapter(listAdapter);

		getActivity().setTitle(property);
		return rootView;
	}
}