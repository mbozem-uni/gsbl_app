package de.ovgu.de.gsbl.view.gui.viewdata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import de.ovgu.de.gsbl.R;
import de.ovgu.de.gsbl.view.datamodel.Field;
import de.ovgu.de.gsbl.view.datamodel.Tab;
import de.ovgu.de.gsbl.view.datamodel.ViewConstants;
import de.ovgu.de.gsbl.view.util.Kurz2LangBezeichnung;
import de.ovgu.de.gsbl.view.util.Kurz2LangInstance;
import de.ovgu.de.gsbl.view.util.ViewUtil;

/**
 * Ergebnisanzeige nach einer Suche nach einem Stoff.
 * 
 * @author Tristan
 * 
 */
public class ViewDataActivity extends Activity {

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	private de.ovgu.de.gsbl.view.datamodel.View defaultView;
	public static String[] mTabs;
	public static List<String>[] dataHeader;
	public static Map<String, List<String>>[] dataChildren;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_data);

		defaultView = (de.ovgu.de.gsbl.view.datamodel.View) getIntent()
				.getSerializableExtra(ViewConstants.DEFAULTVIEW.getName());

		mTitle = mDrawerTitle = getTitle();
		mTabs = defaultView.getTabNames();
		dataHeader = initDataHeader(defaultView.getTabs());
		dataHeader = ViewUtil.castToLongName(dataHeader);
		dataChildren = initDataChildren(defaultView.getTabs());
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);

		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);
		mDrawerList.setAdapter(new ArrayAdapter<String>(this,
				R.layout.drawer_list_item, mTabs));
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		getActionBar().setDisplayHomeAsUpEnabled(true);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_close) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			selectItem(0);
		}
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return false;
	}

	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			selectItem(position);
		}
	}

	private void selectItem(int position) {
		android.app.Fragment fragment = new PropertiesFragment();
		Bundle args = new Bundle();
		args.putInt(PropertiesFragment.ARG_PROPERTIES_NUMBER, position);
		fragment.setArguments(args);

		android.app.FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.content_frame, fragment).commit();

		mDrawerList.setItemChecked(position, true);
		setTitle(mTabs[position]);
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	private List<String>[] initDataHeader(List<Tab> tabs) {
		List<String>[] dataHeader = new LinkedList[tabs.size()];
		int i = 0;
		for (Tab tab : tabs) {
			List<String> tabTitles = new LinkedList<String>();
			List<Field> properties = tab.getProperties();
			for (Field prop : properties) {
				String title = "";
				if (prop.getTitle().contains(".")) {
					title = prop.getTitle().substring(0,
							prop.getTitle().indexOf("."));
				}
				if (!tabTitles.contains(title)) {
					tabTitles.add(title);
				}
			}
			dataHeader[i++] = tabTitles;
		}
		return dataHeader;
	}

	/**
	 * Erstellen der Listeneintraege. Dabei werden die Merkmale aggregiert.
	 * Aggregieren bedeutet, dass die Merkmale, die zur selben Kategorie
	 * gehören, in einen Listeneintrag zusammengefasst werden und in diesem
	 * untereinander aufgeführt werden. Die Merkmale bilden ein Datentupel.
	 */
	private Map<String, List<String>>[] initDataChildren(List<Tab> tabs) {
		Kurz2LangBezeichnung k2l = Kurz2LangInstance.getInstance()
				.getKurz2LangBezeichnung();
		Map<String, List<String>>[] dataChildren = new HashMap[tabs.size()];
		int i = 0;
		boolean isLastElement = false;
		for (Tab tab : tabs) {
			Map<String, List<String>> listDataChild = new HashMap<String, List<String>>();
			List<Field> properties = tab.getProperties();
			List<String> tabTitles = new LinkedList<String>();
			String oldPrefix = "";
			List<String> oldTitle = new ArrayList<String>();

			List<Temp> tmp = new ArrayList<Temp>();
			int counter = 0;
			int index = 0;
			for (Field prop : properties) {
				if (index == properties.size() - 1) {
					isLastElement = true;
				} else {
					isLastElement = false;
				}
				String title = "";
				String subTitle = "";
				if (prop.getTitle().contains(".")) {
					title = prop.getTitle().substring(0,
							prop.getTitle().indexOf("."));
					subTitle = prop.getTitle().substring(
							prop.getTitle().indexOf(".") + 1);
				}
				String shortTitle = title;
				String tmpTitle = k2l.getLangBezeichnung(title);
				if (tmpTitle != null && !tmpTitle.isEmpty()) {
					title = tmpTitle;
				}
				String tmpSubTitle = k2l.getLangBezeichnung(shortTitle,
						subTitle);
				if (tmpSubTitle != null && !tmpSubTitle.isEmpty()) {
					subTitle = tmpSubTitle;
				}
				if ((!oldPrefix.isEmpty() && !oldPrefix.equals(title))
						&& !isLastElement) {
					tabTitles.add(oldPrefix);
					List<String> aggregat = aggregate(counter, tmp);
					listDataChild.put(oldPrefix, aggregat);
					counter = 0;
					tmp.clear();
				} else if ((!oldPrefix.isEmpty() && !oldPrefix.equals(title))
						&& isLastElement) {
					List<String> aggregat = aggregate(counter, tmp);
					listDataChild.put(oldPrefix, aggregat);
					counter = 0;
					tmp.clear();
					counter = 0;
					tmp.clear();
				}

				if (!tabTitles.contains(title)) {
					List<String> content = prop.getContent();
					String c = "";
					counter = content.size();
					int k = 0;
					for (String s : content) {
						c = subTitle + ": " + s;
						content.set(k, c);
						k++;
					}
					tmp.add(new Temp(title, content));
				} else {
					List<String> content = listDataChild.get(title);
					String c = "";
					List<String> tempContent = prop.getContent();
					int k = 0;
					for (String s : tempContent) {
						c = subTitle + ": " + s;
						tempContent.set(k, c);
						k++;
					}
					tmp.add(new Temp(title, tempContent));
					counter = content.size();
					content.set(content.size() - 1,
							content.get(content.size() - 1).concat("\n\n" + c));
				}
				oldPrefix = title;
				oldTitle.add(title + subTitle);
				index++;
			}
			if (isLastElement
					&& !listDataChild.containsKey(tmp.get(tmp.size() - 1)
							.getTitle())) {
				List<String> aggregat = aggregate(counter, tmp);
				listDataChild.put(tmp.get(tmp.size() - 1).getTitle(), aggregat);
			}
			dataChildren[i++] = listDataChild;
		}
		return dataChildren;
	}

	private List<String> aggregate(int counter, List<Temp> tmp) {
		List<String> list = new ArrayList<String>();
		boolean isAggregatable = true;
		for (int j = 0; j < tmp.size() - 1; j++) {
			if (tmp.get(j).getContent().size() != tmp.get(j + 1).getContent()
					.size()) {
				isAggregatable = false;
				break;
			}
		}
		if (isAggregatable) {
			for (int j = 0; j < counter; j++) {
				String s = "";
				for (Temp t : tmp) {
					List<String> content = t.getContent();
					if (!s.isEmpty())
						s = s.concat("\n\n");
					try {
						s = s.concat(content.get(j));
					} catch (IndexOutOfBoundsException e) {
						// ignore
					}
				}
				list.add(s);
			}
		} else {
			for (Temp t : tmp) {
				String s = "";
				List<String> content = t.getContent();
				for (String c : content) {
					if (!s.isEmpty())
						s = s.concat("\n\n");
					try {
						s = s.concat(c);
					} catch (IndexOutOfBoundsException e) {
						// ignore
					}
				}
				list.add(s);
			}
		}
		return list;
	}
}

class Temp {
	private String title;
	private List<String> content;

	public Temp(String title, List<String> content) {
		super();
		this.title = title;
		this.content = content;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<String> getContent() {
		return content;
	}

	public void setContent(List<String> content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "Temp [title=" + title + ", content=" + content + "]";
	}
}