package de.ovgu.de.gsbl.view.gui.viewcreation;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import de.ovgu.de.gsbl.Application;
import de.ovgu.de.gsbl.GSBLActivity;
import de.ovgu.de.gsbl.R;
import de.ovgu.de.gsbl.datamodel.Begriff;
import de.ovgu.de.gsbl.view.datamodel.Content;
import de.ovgu.de.gsbl.view.datamodel.Field;
import de.ovgu.de.gsbl.view.datamodel.Tab;
import de.ovgu.de.gsbl.view.datamodel.ViewConstants;
import de.ovgu.de.gsbl.view.datamodel.ViewCreation;
import de.ovgu.de.gsbl.view.gui.viewcreation.propertyselection.ChoosePropertiesActivity;
import de.ovgu.de.gsbl.view.util.ViewUtil;

/**
 * Bearbeiten von Sichten. Dazu gehoert das Setzen/Aendern des Sichtnamens, das
 * Loeschen der Sicht und das Anlegen von Tabs.
 * 
 * @author Tristan
 * 
 */
public class CreateViewActivity extends ActionBarActivity {

	private static ListView listView;
	private static List<String> values;
	public static List<Tab> tabs;
	public static List<Field> properties;
	private static Tab selectedTab;
	public static CreateViewActivity reference;
	public static String viewTitle;
	private static boolean isPropertyChosen;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_view);
		if (getIntent().getSerializableExtra(ViewConstants.CREATION.getName()) != null) {
			Boolean clear = (Boolean) getIntent().getSerializableExtra(
					ViewConstants.CREATION.getName());
			if (clear && !isPropertyChosen) {
				if (values != null)
					values.clear();
				if (tabs != null)
					tabs.clear();
				if (properties != null)
					properties.clear();
				if (selectedTab != null)
					selectedTab = null;
				if (viewTitle != null)
					viewTitle = null;
			}
			if (!isPropertyChosen)
				enterViewName();
		} else if (getIntent().getSerializableExtra(
				ViewConstants.VIEW.getName()) != null) {
			de.ovgu.de.gsbl.view.datamodel.View view = null;
			if (CreatePropertiesActivity.isTabDeleted) {
				view = CreatePropertiesActivity.editedView;
			} else if (CreatePropertiesActivity.isChanged) {
				view = GSBLActivity.reference.loadView();
			} else {
				view = (de.ovgu.de.gsbl.view.datamodel.View) getIntent()
						.getSerializableExtra(ViewConstants.VIEW.getName());
			}
			init(view, false);
		} else {
			enterViewName();
		}
	}

	@Override
	public void onRestart() {
		super.onRestart();
		values = new LinkedList<String>();

		if (tabs == null || tabs.isEmpty()) {
			tabs = new ArrayList<Tab>();
			createNewTab();
		} else {
			for (Tab tab : tabs) {
				values.add(tab.getTitle());
			}
		}

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, android.R.id.text1, values);
		listView.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.create_view, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		if (itemId == R.id.action_settings) {
			return true;
		} else if (itemId == R.id.newTab) {
			createNewTab();
			return true;
		} else if (itemId == R.id.change_view_title) {
			changeViewTitle();
			return true;
		} else if (itemId == R.id.deleteView) {
			deleteView();
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	private void deleteView() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		alertDialog.setTitle(getString(R.string.delete_alert_title));
		alertDialog.setMessage(getString(R.string.delete_view_alert_message));
		alertDialog.setIcon(R.drawable.warning);
		alertDialog.setPositiveButton(getString(R.string.delete_alert_yes),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						int i = 0;
						List<de.ovgu.de.gsbl.view.datamodel.View> views = ViewCreation
								.getInstance().getCreatedViews();
						for (de.ovgu.de.gsbl.view.datamodel.View view : views) {
							if (view.getName().equals(viewTitle)) {
								views.remove(i);
								break;
							}
							i++;
						}
						GSBLActivity.reference.saveCustomViews();
						finish();
					}
				});
		alertDialog.setNegativeButton(getString(R.string.delete_alert_no),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
		alertDialog.show();
	}

	private void enterViewName() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setMessage(getString(R.string.enter_view));
		final EditText input = new EditText(this);
		alert.setView(input);
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				Editable value = input.getText();
				if (value.toString().isEmpty()) {
					showInformation("Bitte einen Namen eingeben!", 2);
				} else {
					viewTitle = value.toString();
					initView();
				}
			}
		});
		alert.show();
	}

	private void initView() {

		setTitle(getString(R.string.title_activity_create_view) + ": "
				+ viewTitle);

		Application app = (Application) getApplication();
		List<Begriff> defaultFeatures = app.getDatabase().getBegriffe();
		properties = ViewUtil.initDataChildren(defaultFeatures);
		reference = this;
		values = new LinkedList<String>();

		if (tabs == null || tabs.isEmpty()) {
			tabs = new ArrayList<Tab>();
			createNewTab();
		} else {
			for (Tab tab : tabs) {
				values.add(tab.getTitle());
			}
		}

		listView = (ListView) findViewById(R.id.listView1);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, android.R.id.text1, values);
		listView.setAdapter(adapter);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				int itemPosition = position;

				selectedTab = tabs.get(itemPosition);
				GSBLActivity.reference.saveTab(selectedTab);
				tabs.get(itemPosition).setSelected(true);

				int i = 0;
				for (Tab t : tabs) {
					if (i != itemPosition) {
						t.setSelected(false);
					}
					i++;
				}
				startPropertyActivity();
			}
		});
	}

	private void changeViewTitle() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setMessage(getString(R.string.enter_view));
		final EditText input = new EditText(this);
		alert.setView(input);
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				Editable value = input.getText();
				if (value.toString().isEmpty()) {
					showInformation("Bitte einen Namen eingeben!", 2);
				} else {
					ViewCreation.getInstance().setNewViewName(viewTitle,
							value.toString());
					viewTitle = value.toString();
					setTitle(getString(R.string.title_activity_create_view)
							+ ": " + viewTitle);
					GSBLActivity.reference.saveCustomViews();
				}
			}
		});
		alert.show();
	}

	private void createNewTab() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setMessage(getString(R.string.enter_tab));
		final EditText input = new EditText(this);
		alert.setView(input);
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				Editable value = input.getText();
				if (value.toString().isEmpty()) {
					showInformation("Bitte einen Namen eingeben!", 0);
				} else {
					for (Tab t : tabs) {
						t.setSelected(false);
					}
					values.add(value.toString());
					Tab tab = new Tab(value.toString());
					tab.setSelected(true);
					tabs.add(tab);
					selectedTab = tab;
					GSBLActivity.reference.saveTab(selectedTab);
					GSBLActivity.reference.saveCustomViews();
					chooseProperties();
				}
			}
		});
		alert.show();
	}

	@SuppressWarnings("unchecked")
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 1) {
			if (resultCode == RESULT_OK) {
				ArrayList<Field> result = (ArrayList<Field>) data
						.getSerializableExtra(ViewConstants.RESULT.getName());
				List<Field> props = new LinkedList<Field>();
				for (Field f : result) {
					List<Content> listContent = f.getContentObjects();
					List<Content> content = new LinkedList<Content>();
					for (Content con : listContent) {
						if (con.isState())
							content.add(new Content(con.getText(), con
									.isState()));
					}
					Field field = new Field(f.getTitle());
					field.setContentObjects(content);
					props.add(field);
				}
				if (selectedTab == null) {
					selectedTab = GSBLActivity.reference.loadTab();
				}
				selectedTab.setProperties(props);
				boolean wasReplaced = false;
				for (int i = 0; i < tabs.size(); i++) {
					if (tabs.get(i).getTitle().equals(selectedTab.getTitle())) {
						tabs.set(i, selectedTab);
						wasReplaced = true;
					}
				}
				if (!wasReplaced)
					tabs.add(selectedTab);
				de.ovgu.de.gsbl.view.datamodel.View view = new de.ovgu.de.gsbl.view.datamodel.View(
						viewTitle, ViewUtil.copy(tabs));
				int i = ViewCreation.getInstance().getViewIndex(viewTitle);
				if (i != -1) {
					ViewCreation.getInstance().getCreatedViews().set(i, view);
				} else {
					ViewCreation.getInstance().addView(view);
				}
				GSBLActivity.reference.saveView(view);
				GSBLActivity.reference.saveCustomViews();
				init(view, true);
			}
			if (resultCode == RESULT_CANCELED) {
			}
		}
		isPropertyChosen = false;
	}

	private void init(de.ovgu.de.gsbl.view.datamodel.View view,
			boolean deletedSelectedTab) {
		tabs = view.getTabs();
		reference = this;
		if (properties == null || properties.isEmpty()) {
			Application app = (Application) getApplication();
			List<Begriff> defaultFeatures = app.getDatabase().getBegriffe();
			properties = ViewUtil.initDataChildren(defaultFeatures);
		}
		if (selectedTab != null && deletedSelectedTab)
			selectedTab = null;
		viewTitle = view.getName();
		setTitle(getString(R.string.title_activity_create_view) + ": "
				+ viewTitle);
		listView = (ListView) findViewById(R.id.listView1);
		values = new LinkedList<String>();
		for (Tab tab : tabs) {
			values.add(tab.getTitle());
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, android.R.id.text1, values);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				int itemPosition = position;
				selectedTab = tabs.get(itemPosition);
				GSBLActivity.reference.saveTab(selectedTab);
				tabs.get(itemPosition).setSelected(true);
				int i = 0;
				for (Tab t : tabs) {
					if (i != itemPosition) {
						t.setSelected(false);
					}
					i++;
				}

				startPropertyActivity();
			}
		});
		de.ovgu.de.gsbl.view.datamodel.View newView = new de.ovgu.de.gsbl.view.datamodel.View(
				viewTitle, ViewUtil.copy(tabs));
		int index = ViewCreation.getInstance().getViewIndex(viewTitle);
		ViewCreation.getInstance().getCreatedViews().set(index, newView);
		GSBLActivity.reference.saveView(newView);
		GSBLActivity.reference.saveCustomViews();
	}

	private void chooseProperties() {
		isPropertyChosen = true;

		Intent intent = new Intent(CreateViewActivity.this,
				ChoosePropertiesActivity.class);
		startActivityForResult(intent, 1);
	}

	@SuppressWarnings("deprecation")
	private void showInformation(String info, final int action) {
		AlertDialog alertDialog = new AlertDialog.Builder(
				CreateViewActivity.this).create();

		alertDialog.setTitle("Fehlerhafte Aktion");
		alertDialog.setMessage(info);
		alertDialog.setIcon(R.drawable.warning);

		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				switch (action) {
				case 0: {
					createNewTab();
					break;
				}
				case 1: {
					chooseProperties();
					break;
				}
				case 2: {
					enterViewName();
					break;
				}
				}
			}
		});
		alertDialog.show();
	}

	private void startPropertyActivity() {
		Intent intent = new Intent(CreateViewActivity.this,
				CreatePropertiesActivity.class);
		intent.putExtra(ViewConstants.TAB.getName(), selectedTab);
		startActivity(intent);
	}

	protected void actualizeTabs(Tab tab) {
		boolean tabExists = false;
		for (Tab t : tabs) {
			if (t.getTitle().equals(tab.getTitle())) {
				t.setProperties(tab.getProperties());
				tabExists = true;
			}
		}
		if (!tabExists) {
			if (tabs == null) {
				tabs = new LinkedList<Tab>();
			}
			tabs.add(tab);
		}
		ViewCreation.getInstance().removeView(viewTitle);
		de.ovgu.de.gsbl.view.datamodel.View newView = new de.ovgu.de.gsbl.view.datamodel.View(
				viewTitle, ViewUtil.copy(tabs));
		ViewCreation.getInstance().addView(newView);
		GSBLActivity.reference.saveView(newView);
		GSBLActivity.reference.saveCustomViews();
	}

	protected void actualizeTabName(String oldTitle, String newTitle) {
		for (Tab tab : tabs) {
			if (tab.getTitle().equals(oldTitle)) {
				tab.setTitle(newTitle);
				int i = 0;
				for (String value : values) {
					if (value.equals(oldTitle)) {
						values.set(i, newTitle);
						ArrayAdapter<String> adapter = new ArrayAdapter<String>(
								reference, android.R.layout.simple_list_item_1,
								android.R.id.text1, values);
						listView.setAdapter(adapter);
					}
					i++;
				}
				break;
			}
		}
		ViewCreation.getInstance().removeView(viewTitle);
		de.ovgu.de.gsbl.view.datamodel.View newView = new de.ovgu.de.gsbl.view.datamodel.View(
				viewTitle, ViewUtil.copy(tabs));
		ViewCreation.getInstance().addView(newView);
		GSBLActivity.reference.saveView(newView);
		GSBLActivity.reference.saveCustomViews();
	}

}
