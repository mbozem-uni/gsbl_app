package de.ovgu.de.gsbl.view.gui.viewcreation.propertyselection;

import java.util.ArrayList;
import java.util.List;

import android.app.ExpandableListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;

import com.google.gson.Gson;

import de.ovgu.de.gsbl.Application;
import de.ovgu.de.gsbl.R;
import de.ovgu.de.gsbl.datamodel.Begriff;
import de.ovgu.de.gsbl.view.datamodel.Content;
import de.ovgu.de.gsbl.view.datamodel.Field;
import de.ovgu.de.gsbl.view.datamodel.ViewConstants;
import de.ovgu.de.gsbl.view.gui.viewcreation.CreateViewActivity;
import de.ovgu.de.gsbl.view.util.ViewUtil;

/**
 * Anzeige zur Auswahl von Merkmalen fuer einen Tab.
 * 
 * @author Tristan
 * 
 */
public class ChoosePropertiesActivity extends ExpandableListActivity {

	private ExpandableListView list;
	private ArrayList<Field> dataChildren;
	private PropAdapter mAdapter;
	private Button button;
	private List<String> listShortNamesContent;
	private List<String> listShortNamesFields;

	@SuppressWarnings("unchecked")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choose_fields);
		list = this.getExpandableListView();

		Application app = (Application) getApplication();
		List<Begriff> defaultFeatures = app.getDatabase().getBegriffe();

		Object tmp = getIntent().getSerializableExtra(
				ViewConstants.TAB.getName());
		if (tmp != null) {
			dataChildren = (ArrayList<Field>) tmp;
		} else {
			dataChildren = ViewUtil.initDataChildren(defaultFeatures);
		}

		// Elemente mit laufender Nummer versehen. Anhand dieser können die
		// selektierten Langnamen wieder in Kurznamen umgewandelt werden
		listShortNamesContent = new ArrayList<String>();
		listShortNamesFields = new ArrayList<String>();
		for (Field f : dataChildren) {
			List<Content> content = f.getContentObjects();
			for (Content c : content) {
				listShortNamesContent.add(c.getText());
			}
			listShortNamesFields.add(f.getTitle());
		}

		dataChildren = (ArrayList<Field>) ViewUtil.castToLongName(dataChildren);
		List<Integer> removedContent = ViewUtil.listShortNamesContent;
		List<Integer> removedFields = ViewUtil.listShortNamesFields;
		for (Integer i : removedContent) {
			listShortNamesContent.set(i.intValue(), "XXXXX");
		}
		while (listShortNamesContent.contains("XXXXX")) {
			listShortNamesContent.remove("XXXXX");
		}
		for (Integer i : removedFields) {
			listShortNamesFields.set(i.intValue(), "XXXXX");
		}
		while (listShortNamesFields.contains("XXXXX")) {
			listShortNamesFields.remove("XXXXX");
		}

		mAdapter = new PropAdapter(this, list, dataChildren);
		if (savedInstanceState == null)
			list.setAdapter(mAdapter);
		list.setItemsCanFocus(false);
		list.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
		list.setOnChildClickListener(this);
		list.setOnGroupClickListener(new OnGroupClickListener() {

			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				return dataChildren.get(groupPosition).isSelected();
			}

		});

		addListenerOnButton();
	}

	public void addListenerOnButton() {

		button = (Button) findViewById(R.id.button_ok);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				dataChildren = (ArrayList<Field>) ViewUtil.castToShortName(
						listShortNamesContent, listShortNamesFields,
						dataChildren);

				Intent returnIntent = new Intent();
				returnIntent.putExtra(ViewConstants.RESULT.getName(),
						dataChildren);
				setResult(RESULT_OK, returnIntent);

				finish();
			}

		});
		button = (Button) findViewById(R.id.button_cancel);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				finish();
			}

		});
	}

	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {
		Content subcat = dataChildren.get(groupPosition).getContentObjects()
				.get(childPosition);

		// Fall, dass kein Merkmal enthalten ist und der "Fehlertext"
		// lediglich angezeigt wird => Merkmal kann deshalb nicht selektiert
		// werden
		if (subcat.getText().equals(
				CreateViewActivity.reference
						.getString(R.string.no_Property_in_field))) {
			return false;
		}
		subcat.setState(!dataChildren.get(groupPosition).getContentObjects()
				.get(childPosition).isState());
		dataChildren.get(groupPosition).getContentObjects()
				.set(childPosition, subcat);

		boolean isGroupHasSelected = false;
		for (int i = 0; i < dataChildren.get(groupPosition).getContentObjects()
				.size()
				&& !isGroupHasSelected; i++) {
			isGroupHasSelected = dataChildren.get(groupPosition)
					.getContentObjects().get(i).isState();
		}
		Field c = dataChildren.get(groupPosition);
		c.setSelected(isGroupHasSelected);
		dataChildren.set(groupPosition, c);
		// mAdapter.notifyDataSetChanged();

		int position = parent.getFlatListPosition(ExpandableListView
				.getPackedPositionForChild(groupPosition, childPosition));
		parent.setItemChecked(position, subcat.isState());

		return true;
	}

	@SuppressWarnings("unchecked")
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		// restore data
		dataChildren = (ArrayList<Field>) savedInstanceState
				.getSerializable("cat");
		// set new data to adapter and refresh
		mAdapter.refreshList(dataChildren);
	}

	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		dataChildren = (ArrayList<Field>) ViewUtil.castToShortName(
				listShortNamesContent, listShortNamesFields, dataChildren);
		// save data and selection from list to bundle
		savedInstanceState.putSerializable("cat", dataChildren);
		savedInstanceState.putString("sel",
				new Gson().toJson(list.getCheckedItemPositions()).toString());
	}
}