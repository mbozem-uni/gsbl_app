package de.ovgu.de.gsbl.view.gui.viewcreation;

import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import de.ovgu.de.gsbl.R;
import de.ovgu.de.gsbl.view.datamodel.Tab;
import de.ovgu.de.gsbl.view.datamodel.View;
import de.ovgu.de.gsbl.view.datamodel.ViewConstants;
import de.ovgu.de.gsbl.view.datamodel.ViewCreation;

/**
 * Anzeige zur Auswahl von Sichten, die in der CreateViewActivity bearbeitet
 * werden.
 * 
 * @author Tristan
 * 
 */
public class EditViewActivity extends Activity {

	private static ListView listView;
	private static List<String> values;
	public static List<Tab> tabs;
	public static List<String> properties;
	private View selectedView;
	public static CreateViewActivity reference;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_view);

		setTitle(getString(R.string.title_activity_edit_view) + " - "
				+ getString(R.string.choose_editing_view));

		values = new LinkedList<String>();
		final List<View> views = ViewCreation.getInstance().getCreatedViews();
		if (views == null) {
			final AlertDialog alertDialog = new AlertDialog.Builder(this)
					.create();

			alertDialog.setTitle("Achtung");
			alertDialog
					.setMessage("Bitte zunächst mindestens eine Sicht erstellen oder laden, um Sichten bearbeiten zu können.");
			alertDialog.setIcon(R.drawable.warning);

			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					alertDialog.cancel();
					finish();
				}
			});
			alertDialog.show();
		} else {
			for (View view : views) {
				values.add(view.getName());
			}
			listView = (ListView) findViewById(R.id.listView1);

			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_list_item_1, android.R.id.text1,
					values);
			listView.setAdapter(adapter);

			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent,
						android.view.View view, int position, long id) {

					int itemPosition = position;

					CreatePropertiesActivity.isChanged = false;
					selectedView = views.get(itemPosition);
					Intent intent = new Intent(EditViewActivity.this,
							CreateViewActivity.class);
					intent.putExtra(ViewConstants.VIEW.getName(), selectedView);
					startActivity(intent);
				}
			});
		}
	}

	@Override
	public void onRestart() {
		super.onRestart();
		values = new LinkedList<String>();
		final List<View> views = ViewCreation.getInstance().getCreatedViews();
		for (View view : views) {
			values.add(view.getName());
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, android.R.id.text1, values);
		listView.setAdapter(adapter);
	}

}
