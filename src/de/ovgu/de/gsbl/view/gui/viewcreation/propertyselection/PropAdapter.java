package de.ovgu.de.gsbl.view.gui.viewcreation.propertyselection;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.TextView;
import de.ovgu.de.gsbl.R;
import de.ovgu.de.gsbl.view.datamodel.Field;
import de.ovgu.de.gsbl.view.gui.viewcreation.CreateViewActivity;

/**
 * Adaoter zur Anzeige der Merkmale in der Merkmalauswahl fuer einen Tab.
 * 
 * @author Tristan
 * 
 */
@SuppressLint("InflateParams")
public class PropAdapter extends BaseExpandableListAdapter {

	private Context context;
	private List<Field> mGroupCollection;
	private ExpandableListView mExpandableListView;

	public PropAdapter(Context context, ExpandableListView pExpandableListView,
			List<Field> pGroupCollection) {
		this.context = context;
		this.mGroupCollection = pGroupCollection;
		this.mExpandableListView = pExpandableListView;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return mGroupCollection.get(groupPosition).getContentObjects()
				.get(childPosition).getText();
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	class ChildHolder {
		CheckBox checkBox;
		TextView name, desc;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		ChildHolder childHolder;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.activity_test_choose_fields, null);
			childHolder = new ChildHolder();
			childHolder.checkBox = (CheckBox) convertView
					.findViewById(R.id.myCheckBox);
			childHolder.name = (TextView) convertView
					.findViewById(R.id.textView1);
			convertView.setTag(childHolder);
		} else {
			childHolder = (ChildHolder) convertView.getTag();
		}
		childHolder.name.setText(mGroupCollection.get(groupPosition)
				.getContentObjects().get(childPosition).getText());
		// childHolder.desc.setText(mGroupCollection.get(groupPosition).getContentObjects()
		// .get(childPosition).desc);
		childHolder.checkBox.setChecked(mGroupCollection.get(groupPosition)
				.getContentObjects().get(childPosition).isState());

		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return mGroupCollection.get(groupPosition).getContentObjects().size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return mGroupCollection.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return mGroupCollection.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	class GroupHolder {
		TextView title;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		GroupHolder groupHolder;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.activity_choose_fields_grouplayout, null);
			groupHolder = new GroupHolder();
			groupHolder.title = (TextView) convertView.findViewById(R.id.text1);
			convertView.setTag(groupHolder);
		} else {
			groupHolder = (GroupHolder) convertView.getTag();
		}
		groupHolder.title.setText(mGroupCollection.get(groupPosition)
				.getTitle());

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	public void refreshList(List<Field> collection) {
		mGroupCollection = collection;
		notifyDataSetChanged();
		for (int g = 0; g < mGroupCollection.size(); g++) {
			if (mGroupCollection.get(g).isSelected())
				mExpandableListView.expandGroup(g);
			else
				mExpandableListView.collapseGroup(g);
		}
	}
}
