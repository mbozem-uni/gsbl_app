package de.ovgu.de.gsbl.view.gui.viewcreation;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import de.ovgu.de.gsbl.Application;
import de.ovgu.de.gsbl.GSBLActivity;
import de.ovgu.de.gsbl.R;
import de.ovgu.de.gsbl.datamodel.Begriff;
import de.ovgu.de.gsbl.view.datamodel.Content;
import de.ovgu.de.gsbl.view.datamodel.Field;
import de.ovgu.de.gsbl.view.datamodel.Tab;
import de.ovgu.de.gsbl.view.datamodel.View;
import de.ovgu.de.gsbl.view.datamodel.ViewConstants;
import de.ovgu.de.gsbl.view.datamodel.ViewCreation;
import de.ovgu.de.gsbl.view.gui.viewcreation.propertyselection.ChoosePropertiesActivity;
import de.ovgu.de.gsbl.view.util.ViewUtil;

/**
 * Anzeige zur Auswahl von Merkmalen fuer einen Tab.
 * 
 * @author Tristan
 * 
 */
public class CreatePropertiesActivity extends ActionBarActivity {

	private ListView listView;
	private Tab tab;
	private List<String> values;
	private String title;
	public static View editedView;
	public static boolean isTabDeleted;
	public static boolean isChanged;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_properties);

		isTabDeleted = false;
		isChanged = true;

		tab = (Tab) getIntent().getSerializableExtra(
				ViewConstants.TAB.getName());

		title = tab.getTitle();

		setTitle(getString(R.string.tab) + " " + tab.getTitle() + " - "
				+ getString(R.string.title_activity_create_properties));
		listView = (ListView) findViewById(R.id.listView1);

		values = getValues(tab);
		values = ViewUtil.castToLongName(values);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, android.R.id.text1, values);
		listView.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.create_properties, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.changeProperties) {
			changeProperties();
			return true;
		} else if (id == R.id.changeTabName) {
			changeTabName();
			return true;
		} else if (id == R.id.deleteTab) {
			deleteTab();
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	private void setProperties(List<Field> props) {

		List<Field> selectedProperties = new LinkedList<Field>();

		for (Field field : props) {
			List<Content> content = field.getContentObjects();
			Field selectedField = new Field(field.getTitle());
			List<Content> selectedContent = new LinkedList<Content>();
			for (Content c : content) {
				if (c.isState()) {
					selectedContent.add(new Content(c.getText(), c.isState()));
				}
			}
			selectedField.setContentObjects(selectedContent);
			selectedProperties.add(selectedField);
		}

		tab.setProperties(selectedProperties);
		CreateViewActivity.reference.actualizeTabs(tab);
		values = getValues(tab);
		values = ViewUtil.castToLongName(values);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, android.R.id.text1, values);
		listView.setAdapter(adapter);
	}

	@SuppressWarnings("unchecked")
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == 1) {
			if (resultCode == RESULT_OK) {
				ArrayList<Field> result = (ArrayList<Field>) data
						.getSerializableExtra(ViewConstants.RESULT.getName());
				setProperties(result);
				GSBLActivity.reference.saveCustomViews();
			}
			if (resultCode == RESULT_CANCELED) {
			}
		}
	}

	private void changeProperties() {
		List<Field> tabProperties = tab.getProperties();

		Intent intent = new Intent(CreatePropertiesActivity.this,
				ChoosePropertiesActivity.class);
		Application app = (Application) getApplication();
		List<Begriff> defaultFeatures = app.getDatabase().getBegriffe();
		ArrayList<Field> props = ViewUtil.initDataChildren(defaultFeatures);

		for (Field tabP : tabProperties) {
			List<Content> tabC = tabP.getContentObjects();
			for (Content tabContent : tabC) {
				for (Field p : props) {
					if (tabP.getTitle().equals(p.getTitle())) {
						List<Content> c = p.getContentObjects();
						for (Content content : c) {
							if (tabContent.getText().equals(content.getText())) {
								content.setState(true);
							}
						}
					}
				}
			}
		}

		intent.putExtra(ViewConstants.TAB.getName(), props);
		startActivityForResult(intent, 1);
	}

	private void changeTabName() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setMessage(getString(R.string.enter_tab));

		final EditText input = new EditText(this);
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				Editable value = input.getText();
				if (value.toString().isEmpty()) {
					showInformation(
							"Bitte einen Namen für den Reiter eingeben!", 1);
				} else {
					CreateViewActivity.reference.actualizeTabName(
							tab.getTitle(), value.toString());
					tab.setTitle(value.toString());
					title = value.toString();
					setTitle(getString(R.string.tab)
							+ " "
							+ tab.getTitle()
							+ " - "
							+ getString(R.string.title_activity_create_properties));
				}
			}
		});

		alert.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
					}
				});

		alert.show();
	}

	private void deleteTab() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		alertDialog.setTitle(getString(R.string.delete_alert_title));
		alertDialog.setMessage(getString(R.string.delete_alert_message));
		alertDialog.setIcon(R.drawable.warning);
		alertDialog.setPositiveButton(getString(R.string.delete_alert_yes),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						int i = 0;
						List<Tab> tabs = CreateViewActivity.tabs;
						for (Tab tab : tabs) {
							if (tab.getTitle().equals(title)) {
								tabs.remove(i);
								int viewIndex = ViewCreation.getInstance()
										.getViewIndex(
												CreateViewActivity.viewTitle);
								View view = ViewCreation.getInstance()
										.getCreatedViews().get(viewIndex);
								view.getTabs().remove(i);
								ViewCreation.getInstance().getCreatedViews()
										.set(viewIndex, view);
								editedView = view;
								isTabDeleted = true;
								break;
							}
							i++;
						}
						finish();
					}
				});
		alertDialog.setNegativeButton(getString(R.string.delete_alert_no),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
		alertDialog.show();
	}

	@SuppressWarnings("deprecation")
	private void showInformation(String info, final int action) {
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();

		alertDialog.setTitle("Fehlerhafte Aktion");
		alertDialog.setMessage(info);
		alertDialog.setIcon(R.drawable.warning);

		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				switch (action) {
				case 0: {
					changeProperties();
					break;
				}
				case 1: {
					changeTabName();
					break;
				}
				}
			}
		});
		alertDialog.show();
	}

	private List<String> getValues(Tab tab) {
		List<Field> properties = tab.getProperties();
		List<String> values = new LinkedList<String>();
		for (Field prop : properties) {
			List<Content> listContent = prop.getContentObjects();
			for (Content content : listContent) {
				values.add(prop.getTitle() + "." + content.getText());
			}
		}
		return values;
	}
}
