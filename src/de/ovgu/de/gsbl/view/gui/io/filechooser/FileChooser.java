package de.ovgu.de.gsbl.view.gui.io.filechooser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import de.ovgu.de.gsbl.GSBLActivity;
import de.ovgu.de.gsbl.R;

/**
 * FileChooser, der die Dateien eines Verzeichnisses auf dem Endgerät anzeigt.
 * 
 * @author Tristan
 * 
 */
public class FileChooser extends ListActivity {

	private File currentDir;
	private FileArrayAdapter adapter;

	@SuppressLint("SdCardPath")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		currentDir = new File("/sdcard/");
		fill(currentDir);
	}

	/**
	 * Anzeige (FileArrayAdapter) mit den Dateien des gewaehlten Verzeichnisses
	 * fuellen.
	 * 
	 * @param directory
	 *            gewaehltes Verzeichnis
	 */
	private void fill(File directory) {
		File[] dirs = directory.listFiles();
		this.setTitle("Current Dir: " + directory.getName());
		List<Option> dir = new ArrayList<Option>();
		List<Option> fls = new ArrayList<Option>();
		try {
			for (File file : dirs) {
				if (file.isDirectory())
					dir.add(new Option(file.getName(), "Folder", file
							.getAbsolutePath()));
				else {
					fls.add(new Option(file.getName(), "File Size: "
							+ file.length(), file.getAbsolutePath()));
				}
			}
		} catch (Exception e) {

		}
		Collections.sort(dir);
		Collections.sort(fls);
		dir.addAll(fls);
		if (!directory.getName().equalsIgnoreCase("sdcard"))
			dir.add(0,
					new Option("..", "Parent Directory", directory.getParent()));
		adapter = new FileArrayAdapter(FileChooser.this, R.layout.file_view,
				dir);
		this.setListAdapter(adapter);
	}

	/**
	 * Navigieren in gewaehltes Verzeichnis.
	 */
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Option o = adapter.getItem(position);
		if (o.getData().equalsIgnoreCase("folder")
				|| o.getData().equalsIgnoreCase("parent directory")) {
			currentDir = new File(o.getPath());
			fill(currentDir);
		} else {
			onFileClick(o);
		}
	}

	/**
	 * Auswahl einer Datei durch den Benutzer. Laden der Sicht der gewahelten
	 * Datei. Hinzufuegen der Sicht zu den geladenen Sichten der App.
	 * 
	 * @param o
	 *            Option-Objekt, das den Pfad der gewaehlten Datei enthaelt.
	 */
	private void onFileClick(Option o) {

		try {
			String filename = o.getPath();
			File myFile = new File(filename);
			FileInputStream fIn = new FileInputStream(myFile);
			BufferedReader myReader = new BufferedReader(new InputStreamReader(
					fIn));
			String aDataRow = "";
			String aBuffer = "";
			while ((aDataRow = myReader.readLine()) != null) {
				aBuffer += aDataRow + "\n";
			}

			de.ovgu.de.gsbl.view.datamodel.View loadedView = new Gson()
					.fromJson(
							aBuffer,
							new TypeToken<de.ovgu.de.gsbl.view.datamodel.View>() {
							}.getType());
			GSBLActivity.addView(loadedView);

			myReader.close();
			Toast.makeText(
					getBaseContext(),
					"Sicht \""
							+ o.getName().substring(0,
									o.getName().lastIndexOf("."))
							+ "\" geladen.", Toast.LENGTH_LONG).show();
			finish();
		} catch (Exception e) {
			Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG)
					.show();
		}
	}
}