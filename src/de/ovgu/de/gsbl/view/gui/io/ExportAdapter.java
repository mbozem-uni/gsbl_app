package de.ovgu.de.gsbl.view.gui.io;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import de.ovgu.de.gsbl.R;

/**
 * Adapter zur Verwaltung der Listeneintraege fuer die Anzeige der Sichten fuer
 * den Export.
 * 
 * @author Tristan
 * 
 */
public class ExportAdapter extends
		ArrayAdapter<de.ovgu.de.gsbl.view.datamodel.View> {

	private ArrayList<de.ovgu.de.gsbl.view.datamodel.View> viewList;
	private Context context;

	public ExportAdapter(Context context, int textViewResourceId,
			ArrayList<de.ovgu.de.gsbl.view.datamodel.View> views) {
		super(context, textViewResourceId, views);
		this.viewList = new ArrayList<de.ovgu.de.gsbl.view.datamodel.View>();
		this.viewList.addAll(views);
		this.context = context;
	}

	private class ViewHolder {
		CheckBox name;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder = null;
		Log.v("ConvertView", String.valueOf(position));

		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.export_list, null);

			holder = new ViewHolder();
			holder.name = (CheckBox) convertView.findViewById(R.id.checkBox1);
			convertView.setTag(holder);

			holder.name.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					CheckBox cb = (CheckBox) v;
					de.ovgu.de.gsbl.view.datamodel.View selectedView = (de.ovgu.de.gsbl.view.datamodel.View) cb
							.getTag();
					selectedView.setSelected(cb.isChecked());
				}
			});
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		de.ovgu.de.gsbl.view.datamodel.View selectedView = viewList
				.get(position);
		holder.name.setText(selectedView.getName());
		holder.name.setChecked(selectedView.isSelected());
		holder.name.setTag(selectedView);

		return convertView;

	}
}
