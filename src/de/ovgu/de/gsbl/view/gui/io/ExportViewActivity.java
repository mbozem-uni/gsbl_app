package de.ovgu.de.gsbl.view.gui.io;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import de.ovgu.de.gsbl.R;
import de.ovgu.de.gsbl.view.datamodel.View;
import de.ovgu.de.gsbl.view.datamodel.ViewConstants;
import de.ovgu.de.gsbl.view.util.IOOperations;
import de.ovgu.de.gsbl.view.util.ViewUtil;

/**
 * Anzeige der Sichten, die in der App geladen sind und zur Auswahl für den
 * Export auf den externen Telefonspeicher (SD Card) stehen.
 * 
 * @author Pfofe
 * 
 */
public class ExportViewActivity extends Activity {

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_export_view);

		ArrayList<de.ovgu.de.gsbl.view.datamodel.View> views = (ArrayList<de.ovgu.de.gsbl.view.datamodel.View>) getIntent()
				.getSerializableExtra(ViewConstants.VIEWS.getName());

		displayListView(views);
		addListenerOnButton(views);
	}

	private void displayListView(final ArrayList<View> listEntries) {
		ExportAdapter dataAdapter = new ExportAdapter(this,
				R.layout.export_list, listEntries);
		ListView listView = (ListView) findViewById(R.id.listView);
		listView.setAdapter(dataAdapter);
	}

	public void addListenerOnButton(final ArrayList<View> views) {

		Button button = (Button) findViewById(R.id.button_ok);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(android.view.View arg0) {

				// Sicht/en wird/werden auf der SD-Karte bzw. dem externen
				// Telefonspeicher im Ordner gsbl-views im root-Verzeichnis
				// gespeichert
				if (IOOperations.isExternalStorageWritable()) {
					int counter = 0;
					for (View view : views) {
						if (view.isSelected()) {
							IOOperations.writeToSDFile(view);
							counter++;
						}
					}
					String resultMsg = "";
					if (counter == 0)
						resultMsg = "Keine Sicht exportiert";
					else if (counter == 1)
						resultMsg = "Sicht in Ordner gsbl-views exportiert.";
					else if (counter > 1)
						resultMsg = "Sichten in Ordner gsbl-views exportiert";
					Toast.makeText(ExportViewActivity.this, resultMsg,
							Toast.LENGTH_LONG).show();
				} else {
					ViewUtil.showInformation(
							"Keine Schreibberechtigung auf SD-Karte.",
							ExportViewActivity.this);
				}

				finish();
			}

		});
		button = (Button) findViewById(R.id.button_cancel);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(android.view.View arg0) {

				finish();
			}

		});
	}
}