package de.ovgu.de.gsbl.view.gui.io.filechooser;

import java.util.Locale;

/**
 * POJO, das Namen, Dateninhalt und Pfad eines Elementes in einem Verzeichnis
 * auf dem Endgeraet repraesentiert.
 * 
 * @author Tristan
 * 
 */
class Option implements Comparable<Option> {

	private String name;
	private String data;
	private String path;

	public Option(String n, String d, String p) {
		name = n;
		data = d;
		path = p;
	}

	public String getName() {
		return name;
	}

	public String getData() {
		return data;
	}

	public String getPath() {
		return path;
	}

	@Override
	public int compareTo(Option o) {
		if (this.name != null)
			return this.name.toLowerCase(Locale.GERMANY).compareTo(
					o.getName().toLowerCase(Locale.GERMANY));
		else
			throw new IllegalArgumentException();
	}
}
