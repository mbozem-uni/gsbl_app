package de.ovgu.de.gsbl.view.datamodel;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * POJO, das ein Merkmal in der Ergebnisanzeige repraesentiert.
 * 
 * @author Tristan
 * 
 */
public class Field implements Serializable {

	private static final long serialVersionUID = 1851152868035196300L;
	private String title;
	private boolean isSelected;
	private List<Content> content;

	public List<Boolean> getStates() {
		List<Boolean> states = new LinkedList<Boolean>();
		for (Content c : content)
			states.add(c.isState());
		return states;
	}

	public void setState(Boolean object, int index) {
		this.content.set(index, new Content(content.get(index).getText(),
				object));
	}

	public Field(String title, List<String> text) {
		super();
		this.title = title;
		this.isSelected = false;
		this.content = new LinkedList<Content>();
		for (String s : text) {
			this.content.add(new Content(s, false));
		}
	}

	public Field(String title) {
		this.title = title;
		content = new LinkedList<Content>();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void addContent(Content content) {
		if (this.content == null) {
			this.content = new LinkedList<Content>();
		}
		this.content.add(content);
	}

	public List<String> getContent() {
		List<String> text = new LinkedList<String>();
		for (Content c : content) {
			text.add(c.getText());
		}
		return text;
	}

	public List<Content> getContentObjects() {
		return content;
	}

	public void setContentObjects(List<Content> content) {
		this.content = content;
	}

	public void setContent(List<String> content) {
		if (this.content == null)
			this.content = new LinkedList<Content>();
		for (String s : content) {
			this.content.add(new Content(s, false));
		}
	}

	@Override
	public String toString() {
		return "Field [title=" + title + ", content=" + content + "]";
	}

	public Field copy() {
		List<Content> copyContent = new LinkedList<Content>();
		for (Content origin : this.content) {
			copyContent.add(origin.copy());
		}
		Field copyField = new Field(this.title);
		copyField.setContentObjects(copyContent);
		return copyField;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

}
