package de.ovgu.de.gsbl.view.datamodel;

import java.io.Serializable;

/**
 * POJO, das den Inhalt eines Merkmals in der Ergebnisanzeige repraesentiert.
 * 
 * @author Tristan
 * 
 */
public class Content implements Serializable {

	private static final long serialVersionUID = 365689651574789028L;
	private String text;
	private boolean state;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public Content(String text, boolean state) {
		super();
		this.text = text;
		this.state = state;
	}

	@Override
	public String toString() {
		return "Content [text=" + text + ", state=" + state + "]";
	}

	public Content copy() {
		return new Content(this.text, this.state);
	}
}
