package de.ovgu.de.gsbl.view.datamodel;

/**
 * Konstanten für Kommunikation zwischen Activities im Kontext der
 * Sichtenerstelltung und Ergebnisdarstellung in einer Sicht.
 * 
 * @author Tristan
 * 
 */
public enum ViewConstants {

	DEFAULTVIEW("Default View"), CREATION("Create View"), VIEW("View"), VIEWS(
			"Views"), TAB("Tab"), PROPERTY("Property"), RESULT("Result");

	private String name;

	ViewConstants(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}
}
