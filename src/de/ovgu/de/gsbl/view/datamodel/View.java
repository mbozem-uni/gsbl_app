package de.ovgu.de.gsbl.view.datamodel;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * POJO, das eine Sicht repraesentiert.
 * 
 * @author Tristan
 * 
 */
public class View implements Serializable {

	private static final long serialVersionUID = -4740058184584373309L;
	private List<Tab> tabs;
	private String name;
	private boolean isSelected;

	public View(String name, List<Tab> tabs) {
		super();
		this.tabs = tabs;
		this.name = name;
		this.isSelected = false;
	}

	public View(String name) {
		this.name = name;
		this.tabs = new LinkedList<Tab>();
		this.isSelected = false;
	}

	public String[] getTabNames() {
		String[] titles = new String[tabs.size()];
		int i = 0;
		for (Tab tab : tabs) {
			titles[i++] = tab.getTitle();
		}
		return titles;
	}

	public List<Tab> getTabs() {
		return tabs;
	}

	public void setTabs(List<Tab> tabs) {
		this.tabs = tabs;
	}

	public void addTab(String title) {
		if (tabs == null)
			tabs = new LinkedList<Tab>();
		tabs.add(new Tab(title));
	}

	@Override
	public String toString() {
		return "View [tabs=" + tabs + ", name=" + name + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public View copy() {
		List<Tab> copyTabs = new LinkedList<Tab>();
		for (Tab origin : tabs) {
			copyTabs.add(origin.copy());
		}
		return new View(this.name, copyTabs);
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
}
