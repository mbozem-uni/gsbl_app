package de.ovgu.de.gsbl.view.datamodel;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Enthaelt alle zur Laufzeit geladenen Sichten sowie die in den Einstellungen
 * als Standardsicht definierte default view.
 * 
 * @author Tristan
 * 
 */
public class ViewCreation {

	private static ViewCreation instance;
	private CopyOnWriteArrayList<View> createdViews;
	private View defaultView;

	private ViewCreation() {
	}

	public static ViewCreation getInstance() {
		if (instance == null)
			instance = new ViewCreation();
		return instance;
	}

	public CopyOnWriteArrayList<View> getCreatedViews() {
		return createdViews;
	}

	public void setCreatedViews(CopyOnWriteArrayList<View> createdViews) {
		this.createdViews = createdViews;
	}

	public void addView(View view) {
		if (createdViews == null)
			createdViews = new CopyOnWriteArrayList<View>();
		createdViews.add(view);
	}

	public int getViewIndex(String name) {
		int i = 0;
		if (createdViews == null) {
			return -1;
		}
		for (View view : createdViews) {
			if (view.getName().equals(name)) {
				return i;
			}
			i++;
		}
		return -1;
	}

	public void removeView(String name) {
		int i = 0;
		for (View view : createdViews) {
			if (view.getName().equals(name)) {
				createdViews.remove(i);
			}
			i++;
		}
	}

	public void setNewViewName(String oldName, String newName) {
		if (createdViews != null) {
			for (View view : createdViews) {
				if (view.getName().equals(oldName)) {
					view.setName(newName);
				}
			}
		}
	}

	public View getDefaultView() {
		return defaultView;
	}

	public void setDefaultView(View defaultView) {
		this.defaultView = defaultView;
	}

}
