package de.ovgu.de.gsbl.view.datamodel;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * POJO, das einen Tab in der Sichtenerstellung und Sicht zur
 * Ergebnisdarstellung repraesentiert.
 * 
 * @author Tristan
 * 
 */
public class Tab implements Serializable {

	private static final long serialVersionUID = -1561065790465687574L;
	private String title;
	private List<Field> properties;
	private boolean isSelected;

	public Tab(String title) {
		this.title = title;
		properties = new LinkedList<Field>();
	}

	public Tab(String title, List<Field> properties) {
		super();
		this.title = title;
		this.properties = properties;
	}

	public List<Field> getProperties() {
		return properties;
	}

	public void setProperties(List<Field> properties) {
		this.properties = properties;
	}

	public void addProperty(String title) {
		if (properties == null)
			properties = new LinkedList<Field>();
		properties.add(new Field(title));
	}

	public void addPropertyTitles(List<String> titles) {
		if (properties == null)
			properties = new LinkedList<Field>();
		for (String title : titles)
			properties.add(new Field(title));
	}

	public void setPropertyTitles(List<String> titles) {
		if (properties == null)
			properties = new LinkedList<Field>();
		else
			properties.clear();
		for (String title : titles)
			properties.add(new Field(title));
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "Tab [title=" + title + ", properties=" + properties + "]";
	}

	public Tab copy() {
		List<Field> copyFields = new LinkedList<Field>();
		for (Field origin : this.properties) {
			copyFields.add(origin.copy());
		}
		Tab copyTab = new Tab(this.title, copyFields);
		return copyTab;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
}
