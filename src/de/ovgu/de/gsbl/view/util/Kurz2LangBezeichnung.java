package de.ovgu.de.gsbl.view.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import de.ovgu.de.gsbl.datamodel.Begriff;
import de.ovgu.de.gsbl.datamodel.Bezeichnung;

/**
 * Konverter, der zu Kurzbezeichnungen die entsprechenden Langbezeichnungen
 * ermittelt, oder einfach die Begriffe rausfilter, d.h. die Merkmale und
 * zugehörige Felder findet.
 * 
 * @author Christian
 * 
 */
public class Kurz2LangBezeichnung {

	private Kurz2LangBezeichnungMap kurz2LangBezeichnungMap;

	public Kurz2LangBezeichnung(Kurz2LangBezeichnungMap kurz2LangBezeichnungMap) {
		this.kurz2LangBezeichnungMap = kurz2LangBezeichnungMap;
	}

	/**
	 * Hier wird die übergebene merkmalsKurzBezeichnung in der map gesucht, die
	 * mit dem Konstruktor übergeben wurde. Die übergebene KurzBezeichnung
	 * sollte in der Hierarchie der Map an vorletzter Stufe zu finden sein.
	 * 
	 * @param merkmalsKurzBezeichnung
	 * @return
	 */
	public String getLangBezeichnung(String merkmalsKurzBezeichnung) {
		return this.getLangBezeichnung(merkmalsKurzBezeichnung,
				this.kurz2LangBezeichnungMap);
	}

	/**
	 * Hier wird die übergebene merkmalsKurzBezeichnung in der map gesucht, die
	 * mit dem Konstruktor übergeben wurde. Die übergebene KurzBezeichnung
	 * sollte in der Hierarchie der Map an vorletzter Stufe zu finden sein.
	 * Anschließend wird innerhalb des gefundenen Merkmals nach der übergebenen
	 * FeldKurzBezeichnung gesucht und dessen LangBezeichnung zurückgegeben.
	 * 
	 * @param merkmalsKurzBezeichnung
	 * @return
	 */
	public String getLangBezeichnung(String merkmalsKurzBezeichnung,
			String feldKurzBezeichnung) {
		return this.getLangBezeichnung(merkmalsKurzBezeichnung,
				feldKurzBezeichnung, this.kurz2LangBezeichnungMap);
	}

	/**
	 * Hier wird die übergebene merkmalsKurzBezeichnung in der übergebenen map
	 * gesucht. Methode nötig wegen des rekursiven Aufrufs. Die übergebene
	 * KurzBezeichnung sollte in der Hierarchie der Map an vorletzter Stufe zu
	 * finden sein. Anschließend wird innerhalb des gefundenen Merkmals nach der
	 * übergebenen FeldKurzBezeichnung gesucht und dessen LangBezeichnung
	 * zurückgegeben.
	 * 
	 * @param merkmalsKurzBezeichnung
	 * @return
	 */
	private String getLangBezeichnung(String merkmalsKurzBezeichnung,
			String feldKurzBezeichnung,
			Kurz2LangBezeichnungMap kurz2LangBezeichnungMap) {

		// Erfolgsfall, der die REKUSRION beendet
		Set<Bezeichnung> keySetKurz2LangBezeichnungMap = kurz2LangBezeichnungMap
				.keySet();
		for (Bezeichnung bezeichnung : keySetKurz2LangBezeichnungMap) {
			if (bezeichnung.getKurzBezeichnung()
					.equals(merkmalsKurzBezeichnung)) {
				Kurz2LangBezeichnungMap kurz2LangBezeichnungMapMitFeldern = kurz2LangBezeichnungMap
						.get(bezeichnung);
				Set<Bezeichnung> keySetKurz2LangBezeichnungMapMitFeldern = kurz2LangBezeichnungMapMitFeldern
						.keySet();
				for (Bezeichnung feldBezeichnung : keySetKurz2LangBezeichnungMapMitFeldern) {
					if (feldBezeichnung.getKurzBezeichnung().equals(
							feldKurzBezeichnung))
						return feldBezeichnung.getLangBezeichnung();
				}
			}

		}

		// REKURSION!!
		// wurde nichts gefunden, wird jetzt die nächste Stufe überprüft
		Collection<Kurz2LangBezeichnungMap> kurz2LangBezeichnungMaps = kurz2LangBezeichnungMap
				.values();
		for (Kurz2LangBezeichnungMap kurz2LangBezeichnungMapLocal : kurz2LangBezeichnungMaps) {
			String langBezeichnung = getLangBezeichnung(
					merkmalsKurzBezeichnung, feldKurzBezeichnung,
					kurz2LangBezeichnungMapLocal);
			if (langBezeichnung != null)
				return langBezeichnung;
		}

		// Default-Rückgabewert, der die REKURSION weiter laufen lässt:
		// in der übergebenen Map wurde keine Langbezeichnung zur übergebenen
		// Kurzbezeichnung gefunden
		return null;
	}

	/**
	 * Hier wird die übergebene merkmalsKurzBezeichnung in der übergebenen map
	 * gesucht. Methode nötig wegen des rekursiven Aufrufs. Die übergebene
	 * KurzBezeichnung sollte in der Hierarchie der Map an vorletzter Stufe zu
	 * finden sein.
	 * 
	 * @param merkmalsKurzBezeichnung
	 * @return
	 */
	private String getLangBezeichnung(String merkmalsKurzBezeichnung,
			Kurz2LangBezeichnungMap kurz2LangBezeichnungMap) {
		Set<Bezeichnung> keySet = kurz2LangBezeichnungMap.keySet();
		for (Bezeichnung bezeichnung : keySet) {
			if (bezeichnung.getKurzBezeichnung()
					.equals(merkmalsKurzBezeichnung))
				return bezeichnung.getLangBezeichnung();
		}

		// wurde nichts gefunden, wird jetzt die nächste Stufe überprüft
		Collection<Kurz2LangBezeichnungMap> kurz2LangBezeichnungMaps = kurz2LangBezeichnungMap
				.values();
		for (Kurz2LangBezeichnungMap kurz2LangBezeichnungMapLocal : kurz2LangBezeichnungMaps) {
			String langBezeichnung = getLangBezeichnung(
					merkmalsKurzBezeichnung, kurz2LangBezeichnungMapLocal);
			if (langBezeichnung != null)
				return langBezeichnung;
		}

		// in der übergebenen Map wurde keine Langbezeichnung zur übergebenen
		// Kurzbezeichnung gefunden
		return null;
	}

	/**
	 * Ermittelt auf Basis der Map nur die Merkmale und deren Felder in Form von
	 * Begriffen. Dazu wird die Map rekursiv bis zur letzten Ebene abgearbeitet.
	 * 
	 * @return
	 */
	public List<Begriff> getBegriffe() {
		List<Begriff> begriffe = this.getBegriffe(null, this.kurz2LangBezeichnungMap);
		return begriffe;
	}

	private List<Begriff> getBegriffe(Begriff oberbegriff,
			Kurz2LangBezeichnungMap k2lMap) {
		List<Begriff> result = new ArrayList<Begriff>();

		Set<Bezeichnung> bezeichnungen = k2lMap.keySet();
		Bezeichnung bezeichnung = bezeichnungen.iterator().next();
		if (k2lMap.get(bezeichnung) == null || k2lMap.get(bezeichnung).isEmpty()){
			for(Bezeichnung bezeichnung2 : bezeichnungen) {
				oberbegriff.addUnterbegriff(new Begriff(bezeichnung2.getKurzBezeichnung()));
			}
			result.add(oberbegriff);
		} else {
			for(Bezeichnung bezeichnung2 : bezeichnungen) {
				Kurz2LangBezeichnungMap k2LMap2 = k2lMap.get(bezeichnung2);
				result.addAll(getBegriffe(new Begriff(bezeichnung2.getKurzBezeichnung()), k2LMap2));
			}
		}
		return result;
	}

}
