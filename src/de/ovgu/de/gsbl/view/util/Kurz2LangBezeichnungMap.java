package de.ovgu.de.gsbl.view.util;

import java.util.HashMap;
import java.util.List;

import de.ovgu.de.gsbl.datamodel.Bezeichnung;

/**
 * Klasse wird benutzt, um die rekursive Datenstruktur des Datenmodells abbilden zu können
 * 
 * @author Christian
 *
 */
public class Kurz2LangBezeichnungMap extends HashMap<Bezeichnung, Kurz2LangBezeichnungMap> {

	private static final long serialVersionUID = -8615181403063689925L;
	
}
