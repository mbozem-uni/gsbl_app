package de.ovgu.de.gsbl.view.util;

public class Kurz2LangInstance {

	private static Kurz2LangInstance instance;
	private Kurz2LangBezeichnung k2l;

	public static Kurz2LangInstance getInstance() {
		if (instance == null) {
			instance = new Kurz2LangInstance();
		}
		return instance;
	}

	private Kurz2LangInstance() {
	}

	public void setKurz2LangBezeichnung(Kurz2LangBezeichnung k2l) {
		this.k2l = k2l;
	}

	public Kurz2LangBezeichnung getKurz2LangBezeichnung() {
		return this.k2l;
	}
}
