package de.ovgu.de.gsbl.view.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.ovgu.de.gsbl.datamodel.Begriff;
import de.ovgu.de.gsbl.view.datamodel.Content;
import de.ovgu.de.gsbl.view.datamodel.Field;
import de.ovgu.de.gsbl.view.datamodel.Tab;
import de.ovgu.de.gsbl.view.datamodel.ViewConstants;

public class DataGenerator {

	/**
	 * Liefert die Properties zu mehreren Documents, d.h. ein Eintrag in der
	 * zurückgegebenen Liste sind die Properties zu einem Document
	 * 
	 * @return
	 */
	public static List<Map<String, Object>> getProperties() {
		List<Map<String, Object>> properties = new ArrayList<Map<String, Object>>();
		Map<String, Object> property = new HashMap<String, Object>();
		properties.add(property);

		String gsbl = "GSBL";
		Map<String, Object> gsblMap = new HashMap<String, Object>();
		gsblMap.put("GSBLRN", Integer.valueOf(1));
		gsblMap.put("STAR", "|Einzelinhaltsstoff");
		property.put(gsbl, gsblMap);

		String uname = "UNAME";
		List<Map<String, Object>> unameList = new ArrayList<Map<String, Object>>();
		Map<String, Object> unameListMap1 = new HashMap<String, Object>();
		unameListMap1.put("RNAME", "BENZENE");
		unameListMap1.put("GSBLRN", Integer.valueOf(100));
		unameListMap1.put("MKNR", Integer.valueOf(25213));
		unameList.add(unameListMap1);

		Map<String, Object> unameListMap2 = new HashMap<String, Object>();
		unameListMap2.put("RNAME", "BENZOL");
		unameListMap2.put("GSBLRN", Integer.valueOf(100));
		unameListMap2.put("MKNR", Integer.valueOf(25214));
		unameListMap2.put("NOCH_IRGENDWAS", Integer.valueOf(123));
		unameList.add(unameListMap2);
		property.put(uname, unameList);

		String dispname = "DISPNAME";
		Map<String, Object> dispnameMap = new HashMap<String, Object>();
		dispnameMap.put("NAME", "Benzol-Dispname");
		property.put(dispname, dispnameMap);

		String rname = "RNAME";
		Map<String, Object> rnameMap = new HashMap<String, Object>();
		rnameMap.put("RNAME", "Benzol-RNAME");
		rnameMap.put("SPR", "DE");
		property.put(rname, rnameMap);

		// Submerkmale erstmal ignorieren
		// DD = {
		// Submerkmal = {
		// WERT2=199.983, WERT1=1.33322
		// }
		// },
		List<String> bordmMapList = new ArrayList<String>();
		bordmMapList.add("wert1");
		bordmMapList.add("wert2");
		bordmMapList.add("wert3");
		property.put("BORDM", bordmMapList);

		property = new HashMap<String, Object>();
		properties.add(property);

		rname = "RNAME";
		rnameMap = new HashMap<String, Object>();
		rnameMap.put("RNAME", "Benzol-RNAME-DE");
		rnameMap.put("SPR", "DE");
		property.put(rname, rnameMap);

		return properties;
	}

	public static de.ovgu.de.gsbl.view.datamodel.View initTestView() {
		de.ovgu.de.gsbl.view.datamodel.View view = new de.ovgu.de.gsbl.view.datamodel.View(
				"Test-Sicht");

		List<String> tabTitles = new LinkedList<String>();
		tabTitles.add("Gefahren");
		tabTitles.add("Eigenschaften");
		tabTitles.add("Massnahmen");
		ViewUtil.fillViewWithTabTitles(view, tabTitles);

		List<Field> properties = new LinkedList<Field>();
		List<Content> content = new LinkedList<Content>();
		Field field = new Field("UNAME");
		content.add(new Content("RNAME", false));
		content.add(new Content("GSBLRN", false));
		content.add(new Content("MKNR", false));
		field.setContentObjects(content);
		properties.add(field);
		view.getTabs().get(0).setProperties(properties);

		properties = new LinkedList<Field>();
		content = new LinkedList<Content>();
		field = new Field("TEST");
		content.add(new Content("Test", false));
		field.setContentObjects(content);
		properties.add(field);
		view.getTabs().get(1).setProperties(properties);

		properties = new LinkedList<Field>();
		content = new LinkedList<Content>();
		field = new Field("GSBL");
		content.add(new Content("GSBLRN", false));
		field.setContentObjects(content);
		properties.add(field);
		content = new LinkedList<Content>();
		field = new Field("UNAME");
		content.add(new Content("RNAME", false));
		content.add(new Content("MKNR", false));
		field.setContentObjects(content);
		properties.add(field);
		view.getTabs().get(2).setProperties(properties);

		return view;
	}

	public static List<Field> initDefaultProperties() {

		List<Field> properties = new LinkedList<Field>();
		List<String> content = new LinkedList<String>();
		content.add("Summenformel:" + System.getProperty("line.separator")
				+ "C6H2D5N");
		content.add("CAS-Nummer:" + System.getProperty("line.separator")
				+ "4165-61-1");
		content.add("EG-Nummer:" + System.getProperty("line.separator")
				+ "224-015-9");
		content.add("INDEX-Nummer:" + System.getProperty("line.separator")
				+ "612-008-00-7");
		content.add("UN-Nummer:" + System.getProperty("line.separator")
				+ "1547");
		content.add("GSBL-Nummer:" + System.getProperty("line.separator")
				+ "115159");
		properties.add(new Field("Allgemeine Daten", content));
		content = new LinkedList<String>();
		content.add("Schwerer Atemschutz");
		content.add("Vollschutzanzug, gasdicht");
		content.add("Schutzgrad ereignis- und aufgabenbezogen festlegen");
		properties.add(new Field("Eigenschutz", content));
		content = new LinkedList<String>();
		content.add("Untere Explosionsgrenze:"
				+ System.getProperty("line.separator") + "1.3 Vol.-%");
		content.add("Obere Explosionsgrenze:"
				+ System.getProperty("line.separator") + "11 Vol.-%");
		content.add("Dampfdruck:" + System.getProperty("line.separator")
				+ "0.9hPa");
		content.add("Aggregatzustand:" + System.getProperty("line.separator")
				+ "k.A.");
		content.add("Stoffbeschaffenheit:"
				+ System.getProperty("line.separator") + "k.A.");
		content.add("Geruch:"
				+ System.getProperty("line.separator")
				+ "Unangenehmer Geruch; Aromatischer Geruch; Animartiger Geruch; Muffiger Geruch; Fischgeruch");
		content.add("Farbe:"
				+ System.getProperty("line.separator")
				+ "Unter Einwirkung von Luft: verf�rbt sich; Unter Lichteinwirkung: verf�rbt sich");
		content.add("Flammpunkt:" + System.getProperty("line.separator")
				+ "70�");
		content.add("Siedetemperatur:" + System.getProperty("line.separator")
				+ "k.A.");
		content.add("Schmelztemperatur" + System.getProperty("line.separator")
				+ "k.A.");
		properties.add(new Field("Technische Daten", content));

		content = new LinkedList<String>();
		content.add("Gas/Dampf mit Luft explosiv innerhalb der Z�ndgrenzen");
		properties.add(new Field("Explosionsgefahr", content));
		content = new LinkedList<String>();
		content.add("DIREKTE BRANDGEFAHR: Leichtendz�ndlich; Gas/Dampf mit Luft z�ndf�hig innerhalb der Z�ndgrenzen");
		content.add("INDIREKTE BRANDGEFAHR: Kann sich elektrostatisch aufladen mit Entz�ndungsgefahr; M�gliche Entz�ndung durch Funken; Gas/Dampf breitet sich am Boden aus: Z�ndgefahr; Reaktionen mit Feuergefahr: siehe \"Chemische Reaktionen\"");
		properties.add(new Field("Brandgefahr", content));
		content = new LinkedList<String>();
		content.add("Stabilit�t: Stabil unter Normalbedingungen");
		properties.add(new Field("Zersetzung", content));
		content = new LinkedList<String>();
		content.add("Gas/Dampf Verh�ltnis zu Luft: Gas/Dampf schwerer als Luft bei 20�");
		properties.add(new Field("Verhalten der D�mpfe", content));
		content = new LinkedList<String>();
		content.add("Wasserunl�slich");
		content.add("Neutralit�t: Der Stoff reagiert neutral");
		properties.add(new Field("Stoffverhalten in/auf Wasser", content));
		content = new LinkedList<String>();
		content.add("DIREKTE TOXIZIT�TSGEFAHR: DIREKTE TOXIZIT�TSGEFAHR; Gesundheitssch�dlich; Reizwirkung; Der Stoff wirkt auf das Nervensystem");
		content.add("INDIREKTE TOXIZIT�TSGEFAHR: Reaktionen mit Toxizit�tsgefahr: siehe \"Chemische Reaktionen\"");
		content.add("ALLGEMEIN: L�ngerer Exposistion: Gefahr ernster Gesundheitssch�den beim Verschlucken, Ber�hrung mit der Haut und Einatmen; Einwirkung auf das Nervensystem");
		content.add("ALLGEMEINE MA�NAHMEN: Geruchsschwelle ist h�her als der Expositionsgrenzwert");
		properties.add(new Field("Allgemeine Gesundheitsgefahren", content));
		content = new LinkedList<String>();
		content.add("AEGL2 (4h): 400 ppm");
		content.add("Wassergef�hrdungsklasse: 3");
		properties.add(new Field("Einstufungen und Grenzwerte", content));

		content = new LinkedList<String>();
		content.add("Wenn gefahrlos, ausbrennen lassen");
		content.add("Brand nur aus sicherer Entfernung/Deckung bek�mpfen");
		content.add("Beh�lter aus Deckung ausreichend k�hlen");
		content.add("L�schwasser auffangen");
		content.add("Beh�lter m�glichst aus dem Brandbereich entfernen");
		content.add("Auch nach dem L�schen des Brandes weiterk�hlen");
		content.add("Gefahrenbereich absperren");
		content.add("Keinen Vollstrahl auf den Stoff richten");
		properties.add(new Field("Einsatzhinweise bei Brand", content));
		content = new LinkedList<String>();
		content.add("A3F/Lightwater");
		content.add("Schaum");
		content.add("Pulver");
		content.add("Kohlendioxid");
		content.add("L�schmittel auf Umgebung abstimmen");
		properties.add(new Field("L�schmittel", content));
		content = new LinkedList<String>();
		properties.add(new Field("Freisetzung Empfehlung/Ma�nahmen", content));
		properties.add(new Field("Binde- und Neutralisationsmittel", content));
		properties.add(new Field("Abdichtmaterialien", content));
		properties.add(new Field("Verwendung von Wasser", content));
		properties.add(new Field("Messen/Nachweisen (Freisetzung)", content));

		return properties;
	}

	public static List<Begriff> getDefaultBegriffeForSearchItems() {
		List<Begriff> result = new ArrayList<Begriff>();
		addBegriff(result, "Stoff", "GSBL-RN", "Stoffart", "Strukturnummer",
				"Sperre", "Stoffpflegesperre");
		addBegriff(result, "Registriername", "Registriername", "Namensart",
				"Sprachkennung");
		addBegriff(result, "Sonstige Namen", "Name", "Namensart",
				"Sprachkennung", "Namenszusatz");
		addBegriff(result, "Displayname", "Name", "Namensart", "Sprachkennung");
		return result;
	}

	private static void addBegriff(List<Begriff> result, String... begriffe) {
		Begriff begriff = new Begriff(begriffe[0]);
		for (int i = 1; i < begriffe.length; i++) {
			begriff.addUnterbegriff(new Begriff(begriffe[i]));
		}
		result.add(begriff);
	}

	public static de.ovgu.de.gsbl.view.datamodel.View initDefaultView() {
		List<Tab> tabs = new LinkedList<Tab>();

		List<Field> properties = new LinkedList<Field>();
		List<String> content = new LinkedList<String>();
		content.add("Summenformel:" + System.getProperty("line.separator")
				+ "C6H2D5N");
		content.add("CAS-Nummer:" + System.getProperty("line.separator")
				+ "4165-61-1");
		content.add("EG-Nummer:" + System.getProperty("line.separator")
				+ "224-015-9");
		content.add("INDEX-Nummer:" + System.getProperty("line.separator")
				+ "612-008-00-7");
		content.add("UN-Nummer:" + System.getProperty("line.separator")
				+ "1547");
		content.add("GSBL-Nummer:" + System.getProperty("line.separator")
				+ "115159");
		properties.add(new Field("Allgemeine Daten", content));
		content = new LinkedList<String>();
		content.add("Schwerer Atemschutz");
		content.add("Vollschutzanzug, gasdicht");
		content.add("Schutzgrad ereignis- und aufgabenbezogen festlegen");
		properties.add(new Field("Eigenschutz", content));
		content = new LinkedList<String>();
		content.add("Untere Explosionsgrenze:"
				+ System.getProperty("line.separator") + "1.3 Vol.-%");
		content.add("Obere Explosionsgrenze:"
				+ System.getProperty("line.separator") + "11 Vol.-%");
		content.add("Dampfdruck:" + System.getProperty("line.separator")
				+ "0.9hPa");
		content.add("Aggregatzustand:" + System.getProperty("line.separator")
				+ "k.A.");
		content.add("Stoffbeschaffenheit:"
				+ System.getProperty("line.separator") + "k.A.");
		content.add("Geruch:"
				+ System.getProperty("line.separator")
				+ "Unangenehmer Geruch; Aromatischer Geruch; Animartiger Geruch; Muffiger Geruch; Fischgeruch");
		content.add("Farbe:"
				+ System.getProperty("line.separator")
				+ "Unter Einwirkung von Luft: verf�rbt sich; Unter Lichteinwirkung: verf�rbt sich");
		content.add("Flammpunkt:" + System.getProperty("line.separator")
				+ "70�");
		content.add("Siedetemperatur:" + System.getProperty("line.separator")
				+ "k.A.");
		content.add("Schmelztemperatur" + System.getProperty("line.separator")
				+ "k.A.");
		properties.add(new Field("Technische Daten", content));
		tabs.add(new Tab("Eigenschaften", properties));

		properties = new LinkedList<Field>();
		content = new LinkedList<String>();
		content.add("Gas/Dampf mit Luft explosiv innerhalb der Z�ndgrenzen");
		properties.add(new Field("Explosionsgefahr", content));
		content = new LinkedList<String>();
		content.add("DIREKTE BRANDGEFAHR: Leichtendz�ndlich; Gas/Dampf mit Luft z�ndf�hig innerhalb der Z�ndgrenzen");
		content.add("INDIREKTE BRANDGEFAHR: Kann sich elektrostatisch aufladen mit Entz�ndungsgefahr; M�gliche Entz�ndung durch Funken; Gas/Dampf breitet sich am Boden aus: Z�ndgefahr; Reaktionen mit Feuergefahr: siehe \"Chemische Reaktionen\"");
		properties.add(new Field("Brandgefahr", content));
		content = new LinkedList<String>();
		content.add("Stabilit�t: Stabil unter Normalbedingungen");
		properties.add(new Field("Zersetzung", content));
		content = new LinkedList<String>();
		content.add("Gas/Dampf Verh�ltnis zu Luft: Gas/Dampf schwerer als Luft bei 20�");
		properties.add(new Field("Verhalten der D�mpfe", content));
		content = new LinkedList<String>();
		content.add("Wasserunl�slich");
		content.add("Neutralit�t: Der Stoff reagiert neutral");
		properties.add(new Field("Stoffverhalten in/auf Wasser", content));
		content = new LinkedList<String>();
		content.add("DIREKTE TOXIZIT�TSGEFAHR: DIREKTE TOXIZIT�TSGEFAHR; Gesundheitssch�dlich; Reizwirkung; Der Stoff wirkt auf das Nervensystem");
		content.add("INDIREKTE TOXIZIT�TSGEFAHR: Reaktionen mit Toxizit�tsgefahr: siehe \"Chemische Reaktionen\"");
		content.add("ALLGEMEIN: L�ngerer Exposistion: Gefahr ernster Gesundheitssch�den beim Verschlucken, Ber�hrung mit der Haut und Einatmen; Einwirkung auf das Nervensystem");
		content.add("ALLGEMEINE MA�NAHMEN: Geruchsschwelle ist h�her als der Expositionsgrenzwert");
		properties.add(new Field("Allgemeine Gesundheitsgefahren", content));
		content = new LinkedList<String>();
		content.add("AEGL2 (4h): 400 ppm");
		content.add("Wassergef�hrdungsklasse: 3");
		properties.add(new Field("Einstufungen und Grenzwerte", content));
		tabs.add(new Tab("Gefahren", properties));

		properties = new LinkedList<Field>();
		content = new LinkedList<String>();
		content.add("Wenn gefahrlos, ausbrennen lassen");
		content.add("Brand nur aus sicherer Entfernung/Deckung bek�mpfen");
		content.add("Beh�lter aus Deckung ausreichend k�hlen");
		content.add("L�schwasser auffangen");
		content.add("Beh�lter m�glichst aus dem Brandbereich entfernen");
		content.add("Auch nach dem L�schen des Brandes weiterk�hlen");
		content.add("Gefahrenbereich absperren");
		content.add("Keinen Vollstrahl auf den Stoff richten");
		properties.add(new Field("Einsatzhinweise bei Brand", content));
		content = new LinkedList<String>();
		content.add("A3F/Lightwater");
		content.add("Schaum");
		content.add("Pulver");
		content.add("Kohlendioxid");
		content.add("L�schmittel auf Umgebung abstimmen");
		properties.add(new Field("L�schmittel", content));
		content = new LinkedList<String>();
		properties.add(new Field("Freisetzung Empfehlung/Ma�nahmen", content));
		properties.add(new Field("Binde- und Neutralisationsmittel", content));
		properties.add(new Field("Abdichtmaterialien", content));
		properties.add(new Field("Verwendung von Wasser", content));
		properties.add(new Field("Messen/Nachweisen (Freisetzung)", content));
		tabs.add(new Tab("Ma�nahmen", properties));

		de.ovgu.de.gsbl.view.datamodel.View view = new de.ovgu.de.gsbl.view.datamodel.View(
				ViewConstants.DEFAULTVIEW.getName(), tabs);

		return view;
	}

	public static Map<String, Object> initDataFromDatabaseDataModel() {

		Map<String, Object> map = new HashMap<String, Object>();

		Map<String, Object> mapGSBL = new HashMap<String, Object>();
		mapGSBL.put("GSBLRN", 1);
		mapGSBL.put("STAR", "Einzelinhaltsstoff");
		map.put("GSBL", mapGSBL);

		List<Map<String, Object>> mapUNAME = new ArrayList<Map<String, Object>>();
		Map<String, Object> mapRName1 = new HashMap<String, Object>();
		mapRName1.put("RNAME", "BENZENE");
		mapRName1.put("GSBLRN", 100);
		mapRName1.put("MKNR", 25213);
		mapUNAME.add(mapRName1);
		Map<String, Object> mapRName2 = new HashMap<String, Object>();
		mapRName2.put("RNAME", "BENZOL");
		mapRName2.put("GSBLRN", 100);
		mapRName2.put("MKNR", 25214);
		mapRName2.put("TEST", 123);
		mapUNAME.add(mapRName2);
		map.put("UNAME", mapUNAME);

		Map<String, Object> mapDD = new HashMap<String, Object>();
		Map<String, Object> mapSubmerkmal = new HashMap<String, Object>();
		mapSubmerkmal.put("WERT2", 199.983);
		mapSubmerkmal.put("WERT1", 1.33322);
		mapDD.put("Submerkmal", mapSubmerkmal);
		map.put("DD", mapDD);

		List<String> mapBORDM = new ArrayList<String>();
		mapBORDM.add("wert1");
		mapBORDM.add("wert2");
		mapBORDM.add("wert3");
		map.put("BORDM", mapBORDM);

		return map;
	}
}
