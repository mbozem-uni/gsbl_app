package de.ovgu.de.gsbl.view.util;

/**
 * Konstanten fuer die Suche.
 * 
 */
public class Constants {
	public static final String spr = "SPR";
	public static final String de = "DE";
	public static final Object uname = "UNAME";
	public static String dispname = "DISPNAME";
	public static String name = "NAME";
	public static String rname = "RNAME";
}
