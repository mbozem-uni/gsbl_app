package de.ovgu.de.gsbl.view.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import de.ovgu.de.gsbl.R;
import de.ovgu.de.gsbl.datamodel.Begriff;
import de.ovgu.de.gsbl.view.datamodel.Content;
import de.ovgu.de.gsbl.view.datamodel.Field;
import de.ovgu.de.gsbl.view.datamodel.Tab;
import de.ovgu.de.gsbl.view.datamodel.View;
import de.ovgu.de.gsbl.view.gui.viewcreation.CreateViewActivity;

/**
 * Hilfsmethoden zur Sichtenerstellung und -befuellung.
 * 
 * @author Tristan
 * 
 */
public class ViewUtil {

	public static List<Integer> listShortNamesContent;
	public static List<Integer> listShortNamesFields;

	/**
	 * Erstellen des Geruests einer View (als Ergebnis der
	 * Sichtenerstellung/-konfiguration). Die Sicht enthaelt keine Daten in den
	 * Tabs und Properties, lediglich deren Titel werden gesetzt. Die Daten
	 * werden der View zugeordnet, wenn die Daten aus der Datenbank in die
	 * konfigurierte Sicht geladen werden sollen.
	 * 
	 * @param view
	 * @param tabTitles
	 */
	public static void fillViewWithTabTitles(View view, List<String> tabTitles) {
		for (String title : tabTitles) {
			view.addTab(title);
		}
	}

	public static View assignProperties(View existingView,
			Map<String, Object> properties) {
		View returnView = existingView.copy();
		prepareView(returnView);
		iterate(properties, "", returnView);
		fillEmptyFields(returnView);
		return returnView;
	}

	private static void prepareView(View view) {
		List<Tab> tabs = view.getTabs();
		for (Tab tab : tabs) {
			List<Field> fields = tab.getProperties();
			List<Field> newFields = new LinkedList<Field>();
			for (Field field : fields) {
				List<Content> contents = field.getContentObjects();
				for (Content content : contents) {
					newFields.add(new Field(field.getTitle() + "."
							+ content.getText()));
				}
			}
			tab.setProperties(newFields);
		}
	}

	private static void fillEmptyFields(View view) {
		List<Tab> tabs = view.getTabs();
		for (Tab tab : tabs) {
			List<Field> fields = tab.getProperties();
			for (Field field : fields) {
				List<Content> contents = field.getContentObjects();
				if (contents != null && contents.isEmpty()) {
					contents = new LinkedList<Content>();
					contents.add(new Content(
							"Merkmal nicht im Stoff enthalten", false));
					field.setContentObjects(contents);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private static void iterate(Map<?, ?> element, String id, View existingView) {
		Iterator<String> keys = (Iterator<String>) element.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			Object o = element.get(key);
			if (o instanceof Map<?, ?>) {
				if (!id.isEmpty())
					id = id + ".";
				iterate((Map<?, ?>) o, id.concat(key), existingView);
			} else if (o instanceof List<?>) {
				List<?> list = (List<?>) o;
				if (list != null && list.size() > 0) {
					for (Object listElement : list) {
						if (listElement instanceof String) {
							String value = String.valueOf(listElement);
							if (!id.isEmpty())
								id = id + "." + key;
							List<String> values = new LinkedList<String>();
							values.add(value);
							checkWithTabs(id, values, existingView);
							if (id.contains("."))
								id = id.substring(0, id.lastIndexOf("."));
						} else if (listElement instanceof Map<?, ?>) {
							iterate((Map<?, ?>) listElement, id.concat(key),
									existingView);
						}
					}
				}
			} else {
				if (!key.contains("_rev") && !key.contains("_id")
						&& !key.contains("docType")) {
					String value = String.valueOf(o);
					if (!id.isEmpty())
						id = id + ".";
					id = id.concat(key);
					List<String> values = new LinkedList<String>();
					values.add(value);
					checkWithTabs(id, values, existingView);
					if (id.contains("."))
						id = id.substring(0, id.lastIndexOf("."));
				}

			}
		}
	}

	private static void checkWithTabs(String key, List<String> value,
			View existingView) {
		List<Tab> existingTabs = existingView.getTabs();
		for (Tab existingtab : existingTabs) {
			List<Field> existingProperties = existingtab.getProperties();
			for (Field existingProperty : existingProperties) {
				if (existingProperty.getTitle().equals(key)) {
					existingProperty.setContent(value);
				}
			}
		}
	}

	public static ArrayList<Field> initDataChildren(List<Begriff> listBegriffe) {
		Map<String, List<String>> dataChildren = new HashMap<String, List<String>>();
		for (Begriff begriff : listBegriffe) {
			iterateBegriffe(begriff, "", dataChildren);
		}
		Iterator<Entry<String, List<String>>> it = dataChildren.entrySet()
				.iterator();
		ArrayList<Field> listField = new ArrayList<Field>();
		while (it.hasNext()) {
			Entry<String, List<String>> e = it.next();
			listField.add(new Field(e.getKey(), e.getValue()));
		}
		return listField;
	}

	private static void iterateBegriffe(Begriff begriff, String id,
			Map<String, List<String>> dataChildren) {
		if (begriff.isFeld()) {
			List<String> content = dataChildren.get(id);
			if (content == null)
				content = new ArrayList<String>();
			content.add(begriff.getName());
			dataChildren.put(id, content);
		} else {
			String separator = "";
			if (!id.isEmpty())
				separator = ".";

			id = id.concat(separator + begriff.getName());
			List<Begriff> listBegriffe = begriff.getUnterbegriffe();
			for (Begriff b : listBegriffe) {
				iterateBegriffe(b, id, dataChildren);
			}
		}
	}

	public static List<Tab> copy(List<Tab> originTabs) {
		List<Tab> copyTabs = new LinkedList<Tab>();
		for (Tab origin : originTabs) {
			copyTabs.add(origin.copy());
		}
		return copyTabs;
	}

	@SuppressWarnings("deprecation")
	public static void showInformation(String info, Activity act) {
		final AlertDialog alertDialog = new AlertDialog.Builder(act).create();

		alertDialog.setTitle("Achtung");
		alertDialog.setMessage(info);
		alertDialog.setIcon(R.drawable.warning);

		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				alertDialog.cancel();
			}
		});
		alertDialog.show();
	}

	public static List<String> castToLongName(List<String> listShortNames) {
		Kurz2LangBezeichnung k2lb = Kurz2LangInstance.getInstance()
				.getKurz2LangBezeichnung();
		List<String> listLongNames = new ArrayList<String>();
		for (String shortName : listShortNames) {
			String tmpLongName = null;
			String tmpLongNameProperty = null;
			if (shortName.contains(".")) {
				String merkmal = shortName.substring(0, shortName.indexOf("."));
				String feld = shortName.substring(shortName.indexOf(".") + 1);
				tmpLongNameProperty = k2lb.getLangBezeichnung(merkmal);
				tmpLongName = k2lb.getLangBezeichnung(merkmal, feld);
			} else {
				tmpLongName = k2lb.getLangBezeichnung(shortName);
			}
			if (tmpLongName != null && !tmpLongName.isEmpty()) {
				if (tmpLongNameProperty != null
						&& !tmpLongNameProperty.isEmpty()) {
					tmpLongName = tmpLongName + " (" + tmpLongNameProperty
							+ ")";
				}
				listLongNames.add(tmpLongName);
			}
			// Kurnamen, denen kein Langname zugeordnet werden kann, werden
			// gefiltert und nicht angezeigt
			else {
				if (tmpLongNameProperty != null
						&& !tmpLongNameProperty.isEmpty()) {
					shortName = shortName + " (" + tmpLongNameProperty + ")";
				}
				listLongNames.add(shortName);
			}
		}
		return listLongNames;
	}

	@SuppressWarnings("unchecked")
	public static List<String>[] castToLongName(
			List<String>[] listOfArrayShortNames) {
		Kurz2LangBezeichnung k2lb = Kurz2LangInstance.getInstance()
				.getKurz2LangBezeichnung();
		List<String>[] listOfArrayLongNames = new ArrayList[listOfArrayShortNames.length];
		int i = 0;
		for (List<String> listShortNames : listOfArrayShortNames) {
			List<String> listLongNames = new ArrayList<String>();
			for (String shortName : listShortNames) {
				String tmpLongName = k2lb.getLangBezeichnung(shortName);
				if (tmpLongName != null && !tmpLongName.isEmpty()) {
					listLongNames.add(tmpLongName);
				}
				// Kurnamen, denen kein Langname zugeordnet werden kann, werden
				// gefiltert und nicht angezeigt
				// else {
				// listLongNames.add(shortName);
				// }
			}
			listOfArrayLongNames[i] = listLongNames;
			i++;
		}
		return listOfArrayLongNames;
	}

	// Kurnamen, denen kein Langname zugeordnet werden kann, werden
	// gefiltert und nicht angezeigt
	public static List<Field> castToLongName(ArrayList<Field> listShortNames) {
		listShortNamesFields = new ArrayList<Integer>();
		listShortNamesContent = new ArrayList<Integer>();
		Kurz2LangBezeichnung k2lb = Kurz2LangInstance.getInstance()
				.getKurz2LangBezeichnung();
		List<Field> listLongNames = new ArrayList<Field>();
		int indexField = 0;
		int indexContent = 0;
		for (Field f : listShortNames) {
			List<Content> content = f.getContentObjects();
			String title = f.getTitle();
			String longName = k2lb.getLangBezeichnung(title);
			List<Content> contentLongNames = new ArrayList<Content>();
			for (Content c : content) {
				String text = c.getText();
				String longText = k2lb.getLangBezeichnung(title, text);
				if (longText != null && !longText.isEmpty()) {
					contentLongNames.add(new Content(longText, c.isState()));
				} else {
					listShortNamesContent.add(indexContent);
				}
				indexContent++;
			}
			if (longName != null && !longName.isEmpty()) {
				title = longName;
			} else {
				listShortNamesFields.add(indexField);
			}
			Field newField = new Field(title);
			if (contentLongNames.isEmpty()) {
				contentLongNames.add(new Content(CreateViewActivity.reference
						.getString(R.string.no_Property_in_field), false));
			}
			newField.setContentObjects(contentLongNames);
			listLongNames.add(newField);
			indexField++;
		}
		return listLongNames;
	}

	public static List<Field> castToShortName(List<String> shortNamesContent,
			List<String> shortNamesFields, List<Field> longNames) {

		// Info-Texte in leeren Merkmalen entfernen:
		// "Keine Informationen zu diesem Merkmal als Langname vorhanden"
		// entfernen
		for (Field f : longNames) {
			List<Content> listContent = f.getContentObjects();
			if (listContent.size() == 1
					&& listContent
							.get(0)
							.getText()
							.equals(CreateViewActivity.reference
									.getString(R.string.no_Property_in_field))) {
				// listContent.remove(0);
				f.getContentObjects().clear();
			}
		}

		List<Field> returnListShortNames = new ArrayList<Field>();
		int i = 0;
		int j = 0;
		for (Field f : longNames) {
			List<Content> listContent = f.getContentObjects();
			List<Content> newContent = new ArrayList<Content>();
			for (Content c : listContent) {
				c.setText(shortNamesContent.get(i));
				newContent.add(new Content(shortNamesContent.get(i), c
						.isState()));
				i++;
			}
			Field newField = new Field(shortNamesFields.get(j));
			newField.setContentObjects(newContent);
			returnListShortNames.add(newField);
			j++;
		}
		return returnListShortNames;
	}

	public static List<String> castToLongName(String key,
			List<String> listShortNames) {
		Kurz2LangBezeichnung k2lb = Kurz2LangInstance.getInstance()
				.getKurz2LangBezeichnung();
		List<String> listLongNames = new ArrayList<String>();
		for (String shortName : listShortNames) {
			String tmpShortName = shortName
					.substring(0, shortName.indexOf(":"));
			String tmpEnding = shortName.substring(shortName.indexOf(":") + 1);
			String tmpLongName = k2lb.getLangBezeichnung(key, tmpShortName);
			if (tmpLongName != null && !tmpLongName.isEmpty()) {
				listLongNames.add(tmpLongName + tmpEnding);
			} else {
				tmpLongName = k2lb.getLangBezeichnung(tmpShortName);
				if (tmpLongName != null && !tmpLongName.isEmpty()) {
					listLongNames.add(tmpLongName + tmpEnding);
				}
				// Kurnamen, denen kein Langname zugeordnet werden kann, werden
				// gefiltert und nicht angezeigt
				// else {
				// listLongNames.add(shortName);
				// }
			}
		}
		return listLongNames;
	}
}