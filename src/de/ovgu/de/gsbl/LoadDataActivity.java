package de.ovgu.de.gsbl;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.couchbase.lite.Database;

import de.ovgu.de.gsbl.parser.StoffDatenParser;
import de.ovgu.de.gsbl.parser.StoffDatenParser.ParserProgressListener;
import de.ovgu.de.gsbl.parser.XMLDownloadService;

public class LoadDataActivity extends ActionBarActivity {

	private static final String RESULT = "result";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_load_data);

		// falls vorher schon ein Download abgeschlossen wurde, steht das im
		// Intent
		Intent intent = getIntent();
		if (intent.getExtras() != null
				&& intent.getExtras().containsKey(RESULT)) {
			Bundle extras = intent.getExtras();
			String result = (String) extras.get(RESULT);
			TextView textView = new TextView(this);
			textView.setText(result);

			TableRow tableRow = new TableRow(this);
			tableRow.addView(textView);
			TableLayout layout = (TableLayout) findViewById(R.id.loadDataActivity);
			layout.addView(tableRow);
		}

		IntentFilter intentFilter = new IntentFilter(
				XMLDownloadService.xmlDownloadServiceProgress);
		receiver = new XMLDownloadResponseReceiver();
		LocalBroadcastManager.getInstance(this).registerReceiver(receiver,
				intentFilter);
	}

	private XMLDownloadResponseReceiver receiver;

	@Override
	public void onDestroy() {
		super.onDestroy();
		LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
	}

	private void updateFromService(int count, boolean finished) {
		if (finished) {
			((TextView) findViewById(R.id.textStoffeParsed))
					.setText("Der Download ist fertig!");
			Toast.makeText(getBaseContext(),
					getString(R.string.dialog_download_finished),
					Toast.LENGTH_LONG).show();
		} else if (count > 0) {
			((TextView) findViewById(R.id.textStoffeParsed))
					.setText("Erfolgreich verarbeitete Stoffe: " + count);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.load_data, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	// @TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void loadData(View view) {
		EditText editText = (EditText) findViewById(R.id.edit_loadData);
		String editTextString = editText.getText().toString();
		final String xml;
		if (!editTextString.startsWith("http")) {
			xml = "http://" + editTextString;
		} else {
			xml = editTextString;
		}

		/*
		 * Abfrage auf Internetverbindung ist beim Test auf Smartphone
		 * fehlgeschlagen, obwohl gültige WLAN-Verbindung vorlag.
		 */
		// TelephonyManager m = (TelephonyManager)
		// getSystemService(Context.TELEPHONY_SERVICE);

		// if (m.getDataState() != TelephonyManager.DATA_CONNECTED) {
		// this.showDialog(getString(R.string.dialog_no_internet));
		//
		// } else {

		final Activity thisActivity = this;

		AlertDialog.Builder builder = new AlertDialog.Builder(thisActivity);
		builder.setMessage(getString(R.string.dialog_download_will_start));
		builder.setPositiveButton(getString(R.string.dialog_ok),
				new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						Log.d("xmlDownloadService",
								"starting the intent from dialog");
						Intent serviceIntent = new Intent(thisActivity,
								XMLDownloadService.class);
						serviceIntent.putExtra(XMLDownloadService.URL, xml);
						((TextView) findViewById(R.id.textStoffeParsed))
								.setText("Starte Download ...");
						thisActivity.startService(serviceIntent);

						/*
						 * ursprüngliche implementierung mit AsnyTask
						 * de.ovgu.de.gsbl.Application application =
						 * (de.ovgu.de.gsbl.Application) getApplication();
						 * Database db = application.getRawDatabase();
						 * XmlDownloader xmlDownloader = new XmlDownloader(db);
						 * xmlDownloader.execute(xml);
						 */
					}
				});

		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
					}
				});
		// Create the AlertDialog object and return it
		AlertDialog dialog = builder.create();
		dialog.show();

		// }
	}

	// vorlaufig nicht genutzt (da TelephonyManager.DATA_CONNECTED deaktiviert
	// ist)
	/*
	 * private void showDialog(String message) { AlertDialog.Builder builder =
	 * new AlertDialog.Builder(this); builder.setMessage(message);
	 * 
	 * builder.setPositiveButton(getString(R.string.dialog_ok), new
	 * OnClickListener() {
	 * 
	 * @Override public void onClick(DialogInterface dialog, int which) {
	 * 
	 * } }); // Create the AlertDialog object and return it AlertDialog dialog =
	 * builder.create(); dialog.show(); }
	 */

	/* Beginn Code zum downloaden der XML Datei */

	// Implementation of AsyncTask used to download XML
	// AsynTask<ParamsType for doInBackground, Progress Type (i.e. int),
	// ResultType for onPostExecute>
	public class XmlDownloader extends AsyncTask<String, Integer, String>
			implements ParserProgressListener {

		private final Database db;

		public XmlDownloader(Database database) {
			this.db = database;
		}

		final String xmlTAG = "ovgu.xml";

		@Override
		protected String doInBackground(String... urls) {
			try {
				return loadXmlFromNetwork(urls[0]);
			} catch (IOException e) {
				Log.e(xmlTAG, "IOException", e);
				return "IOException";
			} catch (XmlPullParserException e) {
				Log.e(xmlTAG, "XmlPullParserException", e);
				return "XmlPullParserException";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			Toast.makeText(getBaseContext(),
					getString(R.string.dialog_download_finished),
					Toast.LENGTH_LONG).show();
		}

		private String loadXmlFromNetwork(String urlString)
				throws XmlPullParserException, IOException {
			try {
				XmlPullParserFactory factory = XmlPullParserFactory
						.newInstance();
				factory.setNamespaceAware(false);
				XmlPullParser xpp = factory.newPullParser();
				xpp.setInput(new InputStreamReader(getUrlData(urlString)));

				StoffDatenParser stoffDatenParser = new StoffDatenParser(xpp,
						db);
				stoffDatenParser.addProgressListener(this);
				try {
					stoffDatenParser.process();
					stoffDatenParser.removeProgressListener(this);
				} catch (Exception e) {
					Log.e(xmlTAG, "oops", e);
				}
				Log.d(xmlTAG, "xml parsing is done");

			} finally {

			}

			return "done";
		}

		private InputStream getUrlData(String url) {
			try {
				DefaultHttpClient client = new DefaultHttpClient();
				HttpGet method = new HttpGet(new URI(url));
				HttpResponse res = client.execute(method);
				return res.getEntity().getContent();
			} catch (Exception e) {
				Log.e(xmlTAG, "oops", e);
			}
			return null;
		}

		private int parsedCount = 0;

		@Override
		public void stoffHasBeenParsed() {
			parsedCount++;
			publishProgress(Integer.valueOf(parsedCount));
		}

		protected void onProgressUpdate(Integer... progress) {
			TextView theView = (TextView) findViewById(R.id.textStoffeParsed);
			theView.setText("Erfolgreich verarbeitete Stoffe: " + progress[0]);
		}
	}

	/* Ende Code zum downloaden der XML Datei */

	private class XMLDownloadResponseReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			Log.d("xmlDownloadService",
					"onReceive: "
							+ " Progress:"
							+ intent.getIntExtra(
									XMLDownloadService.xmlDownloadServiceProgressCount,
									-1)
							+ " Finished:"
							+ intent.getBooleanExtra(
									XMLDownloadService.xmlDownloadServiceFinished,
									false));

			int count = intent.getIntExtra(
					XMLDownloadService.xmlDownloadServiceProgressCount, -1);
			boolean finished = intent.getBooleanExtra(
					XMLDownloadService.xmlDownloadServiceFinished, false);
			LoadDataActivity.this.updateFromService(count, finished);

		}

	}
}
